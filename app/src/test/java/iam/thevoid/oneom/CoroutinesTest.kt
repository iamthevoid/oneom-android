package iam.thevoid.oneom

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Test

class CoroutinesTest {

    @Test
    fun testNotBlocking() {
//        val start = System.currentTimeMillis()
//        GlobalScope.launch {
//            Thread.sleep(2000)
//            println("Non blocking ${System.currentTimeMillis() - start} ${Thread.currentThread().name}")
//        }
//
//        GlobalScope.run {
//            Thread.sleep(1000)
//            println("Blocking ${System.currentTimeMillis() - start} ${Thread.currentThread().name}")
//        }
//
//        GlobalScope.run {
//            Thread.sleep(2000)
//            println("Blocking 2 ${System.currentTimeMillis() - start} ${Thread.currentThread().name}")
//        }
    }

    @Test
    fun testBlocking() {
//        val start = System.currentTimeMillis()
//        GlobalScope.launch(Dispatchers.IO) {
//            Thread.sleep(2000)
//            println("Non blocking ${System.currentTimeMillis() - start} ${Thread.currentThread().name}")
//        }
//
//        runBlocking {
//            GlobalScope.launch {
//
//            }
//        }
//
//        GlobalScope.run {
//            Thread.sleep(1000)
//            println("Blocking ${System.currentTimeMillis() - start} ${Thread.currentThread().name}")
//        }
//
//        GlobalScope.run {
//            Thread.sleep(2000)
//            println("Blocking 2 ${System.currentTimeMillis() - start} ${Thread.currentThread().name}")
//        }
    }

}