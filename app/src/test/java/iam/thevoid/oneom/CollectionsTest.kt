package iam.thevoid.oneom

import org.junit.Assert
import org.junit.Test

class CollectionsTest {

    object AnySingleInstance

    @Test
    fun testAddInstanceToList() {
        val list1 = mutableListOf(AnySingleInstance, AnySingleInstance, AnySingleInstance)
        Assert.assertEquals(3, list1.size)
        val list2 = mutableListOf<Any>().apply {
            add(AnySingleInstance)
            add(AnySingleInstance)
            add(1)
            add("1")
            add(AnySingleInstance)
        }
        Assert.assertEquals(5, list2.size)
    }
}