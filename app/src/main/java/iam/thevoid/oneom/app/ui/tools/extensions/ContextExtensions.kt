package iam.thevoid.oneom.app.ui.tools.extensions

import android.content.Context
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import iam.thevoid.ae.asFragmentActivity

inline fun <reified T : DialogFragment> Context.dismissDialogFragment() {
    asFragmentActivity().run { supportFragmentManager.dismissDialogFragment<T>() }
}

fun <T : DialogFragment> FragmentManager.dismissDialogFragment(): Unit =
        fragments.forEach {
            if ((it as? T) != null) {
                it.dismiss()
            }
            it.childFragmentManager.dismissDialogFragment<T>()
        }

fun Context.appStartTime() =
    packageManager.getPackageInfo(packageName, 0).run {
        lastUpdateTime
//        firstInstallTime == lastUpdateTime
    }
