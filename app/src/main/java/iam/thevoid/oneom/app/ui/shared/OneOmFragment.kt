package iam.thevoid.oneom.app.ui.shared

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class OneOmFragment<Root : View>(@LayoutRes layoutRes: Int) : Fragment(layoutRes) {

    open fun Root.onRootView() = Unit

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as? Root)?.onRootView()
    }
}