package iam.thevoid.oneom.app.ui.tools.extensions

import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import iam.thevoid.noxml.coroutines.extensions.view.addSetter
import iam.thevoid.noxml.coroutines.extensions.view.setAlpha
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

fun <V : View> ViewGroup.onView(@IdRes id : Int, action : V.() -> Unit): V =
        findViewById<V>(id).apply(action)

fun View.disabled(disabled : Flow<Boolean>) =
        addSetter(disabled) { disabled(it) }

fun View.disabled(disabled: Boolean) {
        alpha = if (disabled) 0.3f else 1f
}
