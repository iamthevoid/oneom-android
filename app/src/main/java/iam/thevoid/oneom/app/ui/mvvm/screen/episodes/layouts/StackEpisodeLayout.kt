package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import iam.thevoid.ae.hide
import iam.thevoid.ae.setRippleClickAnimation
import iam.thevoid.noxml.coroutines.extensions.textview.setText
import iam.thevoid.noxml.coroutines.extensions.view.hide
import iam.thevoid.noxml.coroutines.extensions.view.setOnClickListener
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.app.ui.view.SourceCounter
import iam.thevoid.oneom.app.ui.view.setCount
import iam.thevoid.oneom.data.model.Episode
import kotlinx.coroutines.flow.map

class StackEpisodeLayout(
    private val vm: EpisodesViewModel,
    parent: ViewGroup
) : SplittiesLayout<ConstraintLayout, EpisodesViewModel.StackEpisode>(parent) {
    override val layoutRes: Int
        get() = R.layout.layout_episode_stack_item

    override fun ConstraintLayout.onRootView() {
        setRippleClickAnimation()

        setOnClickListener(itemChanges.map { episode ->
            View.OnClickListener {
                vm.onSourcesClick(episode.episode)
            }
        })

        onView<TextView>(R.id.title) {
            setText(itemChanges.map { (episode) ->
                "${episode.se()} ${episode.title}"
            })
        }

        onView<SourceCounter>(R.id.button_related_source) {
            setCount(itemChanges.map { it.episode })
        }

        onView<View>(R.id.divider) {
            hide(itemChanges.map { it.isLast })
        }
    }
}