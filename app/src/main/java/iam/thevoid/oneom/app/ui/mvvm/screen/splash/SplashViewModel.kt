package iam.thevoid.oneom.app.ui.mvvm.screen.splash

import androidx.lifecycle.ViewModel
import iam.thevoid.oneom.BuildConfig
import iam.thevoid.oneom.app.ui.mvvm.Router
import iam.thevoid.oneom.data.dataset.ConfigArchive
import iam.thevoid.util.WeakDelegate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception

class SplashViewModel(val archive: ConfigArchive) : ViewModel(), CoroutineScope by CoroutineScope(IO) {

    val version = BuildConfig.VERSION_NAME

    var router by WeakDelegate<Router>()

    init {
        getConfig { router?.goMain() }
    }

    private fun getConfig(onComplete: () -> Unit) {
        launch {
            archive.init()
            launch(Main) {
                onComplete()
            }
        }
    }
}