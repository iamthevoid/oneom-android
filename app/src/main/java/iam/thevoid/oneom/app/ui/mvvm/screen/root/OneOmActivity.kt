package iam.thevoid.oneom.app.ui.mvvm.screen.root

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import iam.thevoid.noxml.core.mvvm.viewModel
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.Router

class OneOmActivity : AppCompatActivity(R.layout.activity_one_om), Router {

    private val vm by viewModel<OneOmViewModel>()

    private val navController
        get() = findNavController(R.id.nav_host_fragment)

    override fun goMain() {
        navController.navigate(R.id.action_splashFragment_to_episodesFragment)
    }
}
