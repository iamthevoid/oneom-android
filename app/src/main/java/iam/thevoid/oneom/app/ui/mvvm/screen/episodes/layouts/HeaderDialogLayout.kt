@file:OptIn(ExperimentalCoroutinesApi::class)

package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import iam.thevoid.noxml.coroutines.extensions.textview.setTextResource
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map

class HeaderDialogLayout(parent: ViewGroup) :
    SplittiesLayout<FrameLayout, EpisodesViewModel.SourceHeader>(parent) {

    override val layoutRes: Int
        get() = R.layout.layout_episode_sources_header

    override fun FrameLayout.onRootView() {
        onView<TextView>(R.id.title) {
            setTextResource(itemChanges.map {
                when (it) {
                    EpisodesViewModel.SourceHeader.Torrent -> R.string.sources_title_torrent
                    EpisodesViewModel.SourceHeader.Subtitle -> R.string.sources_title_subtitle
                    EpisodesViewModel.SourceHeader.Online -> R.string.sources_title_online
                }
            })
        }
    }

}