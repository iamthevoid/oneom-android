package iam.thevoid.oneom.app.ui.tools.extensions

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment

inline fun <reified T : DialogFragment> Fragment.showDialogFragment(dialogFragment: T) {
    dialogFragment.show(childFragmentManager, dialogFragment::class.java.canonicalName)
}