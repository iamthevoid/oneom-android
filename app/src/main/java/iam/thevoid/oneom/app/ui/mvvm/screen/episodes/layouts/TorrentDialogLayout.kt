@file:OptIn(ExperimentalCoroutinesApi::class)

package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import iam.thevoid.ae.quantityString
import iam.thevoid.ae.setRippleClickAnimation
import iam.thevoid.e.safe
import iam.thevoid.noxml.coroutines.extensions.textview.setText
import iam.thevoid.noxml.coroutines.extensions.textview.setTextColorResource
import iam.thevoid.noxml.coroutines.extensions.view.hide
import iam.thevoid.noxml.coroutines.extensions.view.setOnClickListener
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.extensions.intent
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.data.model.Torrent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map

class TorrentDialogLayout(private val vm: EpisodesViewModel, parent: ViewGroup) : SplittiesLayout<ConstraintLayout, Torrent>(parent) {

    override val layoutRes: Int
        get() = R.layout.layout_episode_sources_torrent

    override fun ConstraintLayout.onRootView() {

        setRippleClickAnimation()

        setOnClickListener(itemChanges.map { torrent ->
            View.OnClickListener {
                context.startActivity(torrent.intent())
            }
        })

        onView<TextView>(R.id.title) {
            setText(itemChanges.map { it.title })
        }

        onView<TextView>(R.id.lang) {
            setText(itemChanges.map {
                it.lang.short.safe()
            })
            hide(itemChanges.map { it.lang.short.isNullOrBlank() })
        }

        onView<TextView>(R.id.quality) {
            setText(itemChanges.map { it.quality.name.safe() })
            hide(itemChanges.map { it.quality.name.isNullOrBlank() })
        }

        onView<TextView>(R.id.size) {
            setText(itemChanges.map { "${it.size}" })
        }

        onView<TextView>(R.id.seeders) {
            setText(itemChanges.map { quantityString(R.plurals.sources_torrent_seeders, it.seed, it.seed) })
            setTextColorResource(itemChanges.map { if (it.seed > 0) R.color.green else R.color.light_black_50 })
        }
    }

}