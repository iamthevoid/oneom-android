package iam.thevoid.oneom.app.ui.tools.extensions

import android.widget.ImageView
import coil.load
import iam.thevoid.ae.color
import iam.thevoid.noxml.coroutines.extensions.view.addSetter
import iam.thevoid.oneom.R
import kotlinx.coroutines.flow.Flow

fun ImageView.setPoster(url: Flow<String>) = addSetter(url) {
    load(it) {
        listener(
                onStart = {
                    setBackgroundColor(color(R.color.white))
                },
                onError = { _, _ ->
                    setBackgroundResource(R.drawable.backround_solid_white_control_color_stroke_8dp)
                }
        )
        error(R.drawable.ic_local_movie)
    }
}