package iam.thevoid.oneom.app.ui.tools.extensions

import android.content.Intent
import android.net.Uri
import iam.thevoid.oneom.data.model.Torrent

fun Torrent.intent() : Intent {
    val intentMagnet = value?.let(Uri::parse)?.let(Intent(Intent.ACTION_VIEW)::setData)
    val intentPage = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    return Intent.createChooser(intentPage, title).apply {
        if (intentMagnet != null)
            putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(intentMagnet))
    }
}