package iam.thevoid.oneom.app.ui.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment

abstract class OneOmDialogFragment<Root : View>(@LayoutRes val layoutRes: Int) : DialogFragment() {

    companion object {
        const val NO_STYLE = -1
    }

    open fun Root.onRootView() = Unit

    open val style
        get() = android.R.style.Theme_Translucent_NoTitleBar_Fullscreen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (style != NO_STYLE)
            setStyle(STYLE_NORMAL, style)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as? Root)?.onRootView()
    }
}