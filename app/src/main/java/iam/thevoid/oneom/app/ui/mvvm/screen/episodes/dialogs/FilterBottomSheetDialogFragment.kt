package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs

import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import iam.thevoid.noxml.core.mvvm.parentViewModel
import iam.thevoid.noxml.recycler.setItems
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.OneOmBottomSheetDialogFragment
import iam.thevoid.oneom.app.ui.tools.episodeFiltersBindings
import iam.thevoid.oneom.app.ui.tools.extensions.onView

class FilterBottomSheetDialogFragment : OneOmBottomSheetDialogFragment<FrameLayout>() {

    private val vm by parentViewModel<EpisodesViewModel>()

    override val layoutRes: Int
        get() = R.layout.bottom_sheet_fragment_episodes_filter

    override fun FrameLayout.onRootView() {
        onView<RecyclerView>(R.id.episodes_filter_recycler) {
            layoutManager = LinearLayoutManager(context)
            setItems(Filters.values().toList(), episodeFiltersBindings(vm))
        }
    }
}

enum class Filters {
    RELEASED,
    FUTURE,
    FOR_DATE
}