package iam.thevoid.oneom.app.util

import android.app.ActivityManager
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.util.DisplayMetrics
import android.view.WindowManager
import iam.thevoid.e.format
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

object Device {

    val SDK = Build.VERSION.SDK_INT
    val MODEL: String = Build.MODEL
    val BRAND: String = Build.BRAND
    val CPU: Array<String> = Build.SUPPORTED_ABIS
    val OS_VERSION: String = Build.VERSION.RELEASE
    var DEVICE_TYPE: String = ""
    var DISPLAY_INFO: String = ""
    var RAM_MEGABYTES: Long = 0

    fun init(context: Context) {
        DEVICE_TYPE = deviceType(context)
        RAM_MEGABYTES = ramMegabytes(context)
        DISPLAY_INFO = displayInfo(context)
    }

    private fun deviceType(context: Context): String {
        val metrics = DisplayMetrics()
        (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.getMetrics(metrics)
        val widthPixels = metrics.widthPixels
        val heightPixels = metrics.heightPixels
        val scaleFactor = metrics.density
        val widthDp = widthPixels / scaleFactor
        val heightDp = heightPixels / scaleFactor
        val smallestWidth = min(widthDp, heightDp)
        return if (smallestWidth > 720 || smallestWidth > 600) {
            "tablet"
        } else
            "phone"

    }

    private fun ramMegabytes(context: Context): Long {
        val mi = ActivityManager.MemoryInfo()
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activityManager.getMemoryInfo(mi)
        return mi.totalMem / 1048576L
    }

    private fun displayInfo(context: Context): String {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val metrics = DisplayMetrics()
        val display = wm.defaultDisplay
        display.getMetrics(metrics)
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y
        val density = metrics.densityDpi

        val x = (width / metrics.xdpi).toDouble().pow(2.0)
        val y = (height / metrics.ydpi).toDouble().pow(2.0)
        val screenInches = sqrt(x + y)

        return "${width}x$height ${density}dpi ${screenInches.format(2)}\""
    }
}
