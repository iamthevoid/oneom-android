package iam.thevoid.oneom.app

import android.app.Application
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.mvvm.screen.splash.SplashViewModel
import iam.thevoid.oneom.app.util.Device
import iam.thevoid.oneom.app.util.ErrorHandler
import iam.thevoid.oneom.data.DataLayer
import org.koin.dsl.module
import org.koin.dsl.single

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        DataLayer.init(this, appModule)
        Device.init(this)
        Thread.setDefaultUncaughtExceptionHandler(ErrorHandler.ErrorHandlerDelegate(Thread.getDefaultUncaughtExceptionHandler()))
    }

    private val appModule by lazy {
        module {
            single<SplashViewModel>()
        }
    }
}