package iam.thevoid.oneom.app.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.days

object Time {

    const val SECOND = 1000L
    const val MINUTE = SECOND * 60L
    const val HOUR = MINUTE * 60L
    const val DAY = HOUR * 24L
//    const val MONTH = DAY * 30L
//    const val YEAR = MONTH * 12L

    @OptIn(ExperimentalTime::class)
    private fun Date.withOffset(offset : Int, duration :  Duration) =
            Date(time + offset * duration.inMilliseconds.toLong())

    @OptIn(ExperimentalTime::class)
    fun dayStartMillis(offset : Int = 0) = parse(format(Date().withOffset(offset, 1.days), TimeFormat.IDN), TimeFormat.IDN).time

    fun dayEndMillis(offset : Int = 0) = dayStartMillis(offset) + DAY - 1L

    fun parse(date: String?, timeFormat: TimeFormat): Date {
        return if (date == null) {
            Date(0)
        } else try {
            timeFormat.dateFormat().parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            parse(null, timeFormat)
        }
    }

//    @Throws(ParseException::class)
//    fun getYear(date: Date?): Date {
//        return TimeFormat.YEAR.dateFormat().parse(TimeFormat.YEAR.dateFormat().format(date))
//    }

    fun format(date: Date?, out: TimeFormat): String {
        return if (date == null) {
            ""
        } else out.dateFormat().format(date)
    }

//    fun format(date: Date?, out: String?): String {
//        return if (date == null) {
//            ""
//        } else SimpleDateFormat(out).format(date)
//    }

//    fun format(date: Long, out: String?): String {
//        return SimpleDateFormat(out).format(Date(date))
//    }

//    fun format(date: Long, out: TimeFormat): String {
//        return out.dateFormat().format(Date(date))
//    }

//    fun format(date: Long?, out: String?): String {
//        return format(date ?: 0, out)
//    }

//    fun format(date: Long?, out: TimeFormat): String {
//        return format(date ?: 0, out)
//    }

//    @Throws(ParseException::class)
//    fun format(date: String?, `in`: TimeFormat, out: TimeFormat): String {
//        val d = `in`.dateFormat().parse(date)
//        return out.dateFormat().format(d)
//    }

//    fun toString(millis: Long): String {
//        return format(Date(millis), TimeFormat.IDN)
//    }

    enum class TimeFormat(var sdf: SimpleDateFormat) {
        IDN(SimpleDateFormat("yyyy-MM-dd")),
        TEXT(SimpleDateFormat("dd MMMM, yyyy")),
        YEAR(SimpleDateFormat("yyyy")),
        ITDN(SimpleDateFormat("kk:mm:ss")),
        OutputDTwTZ(SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z")),
        OutputDT(SimpleDateFormat("yyyy-MM-dd HH:mm:ss")),
        IDTwTZ(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"));

        fun dateFormat(): SimpleDateFormat {
            return sdf
        }

    }
}