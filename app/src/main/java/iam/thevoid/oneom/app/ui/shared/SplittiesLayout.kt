package iam.thevoid.oneom.app.ui.shared

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import iam.thevoid.noxml.coroutines.adapterview.CoroutinesLayout
import splitties.views.inflate

abstract class SplittiesLayout<Root : View, T>(parent : ViewGroup) : CoroutinesLayout<T>(parent) {

    @get:LayoutRes
    abstract val layoutRes : Int

    open fun Root.onRootView() = Unit

    private var contentView : View? = null

    override fun createView(parent: ViewGroup): View =
            (contentView ?: parent.inflate<Root>(layoutRes, false)
                    .also {
                        contentView = it
                        it.onRootView()
                    })

}