package iam.thevoid.oneom.app.ui.tools

import android.content.Intent

fun shareTextIntent(text: String): Intent = Intent(Intent.ACTION_SEND).apply {
    putExtra(Intent.EXTRA_TEXT, text)
    type = "text/plain"
}
