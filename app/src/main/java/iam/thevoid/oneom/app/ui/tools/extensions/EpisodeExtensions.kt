package iam.thevoid.oneom.app.ui.tools.extensions

import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.data.model.Episode
import iam.thevoid.oneom.data.model.Poster
import java.text.DateFormat

fun Episode.shareText(dateFormat: DateFormat, poster: Poster?): String {
    fun ifExists(string: String?, prefix: String = "\n\n") =
        string?.takeIf { it.isNotBlank() }.let { if (it == null) "" else "$prefix$it" }


    return """
${ifExists(poster?.url())}
${share(dateFormat)}
""".trimMargin().trim()
}

fun EpisodesViewModel.Stack.shareText(dateFormat: DateFormat, poster: Poster?): String {
    return """
${ifExists(poster?.url())}
${episodes.joinToString(separator = "\n") { it.episode.share(dateFormat) }}
""".trimMargin().trim()
}


private fun Episode.share(dateFormat: DateFormat): String {
    return """
${dateFormat.format(airDate)}

[${se()} $title](${url()})${ifExists(description())}
    """.trimIndent()
}

private fun ifExists(string: String?, prefix: String = "\n\n") =
    string?.takeIf { it.isNotBlank() }.let { if (it == null) "" else "$prefix$it" }