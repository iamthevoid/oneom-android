package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.app.DatePickerDialog
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RadioButton
import android.widget.TextView
import iam.thevoid.ae.setRippleClickAnimation
import iam.thevoid.e.dayOfMonth
import iam.thevoid.e.month
import iam.thevoid.e.year
import iam.thevoid.noxml.coroutines.extensions.radiobutton.setChecked
import iam.thevoid.noxml.coroutines.extensions.textview.setTextResource
import iam.thevoid.noxml.coroutines.extensions.view.setOnClickListener
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs.FilterBottomSheetDialogFragment
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs.Filters
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.extensions.dismissDialogFragment
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.data.dataset.episodes.EpisodesFilter
import iam.thevoid.oneom.data.dataset.episodes.FeedState
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import java.util.*


class EpisodeFilterLayout(private val vm: EpisodesViewModel, viewGroup: ViewGroup) :
    SplittiesLayout<FrameLayout, Filters>(viewGroup) {
    override val layoutRes: Int
        get() = R.layout.layout_episode_filter

    override fun FrameLayout.onRootView() {

        setRippleClickAnimation()

        setOnClickListener(combine(itemChanges, vm.filter) { (filters, selected) ->
            View.OnClickListener {
                if (filters !is Filters) return@OnClickListener
                if (selected !is EpisodesFilter) return@OnClickListener
                when {
                    filters == Filters.FUTURE && filters == Filters.RELEASED ->
                        onFilterApplied(EpisodesFilter.Feed(FeedState.valueOf(filters.name)))
                    filters == Filters.FOR_DATE && selected is EpisodesFilter.ForDate ->
                        selectDate(selected.date)
                }
            }
        })

        onView<TextView>(R.id.text) {
            setTextResource(itemChanges.map {
                when (it) {
                    Filters.FUTURE -> R.string.episodes_future
                    Filters.RELEASED -> R.string.episodes_released
                    Filters.FOR_DATE -> R.string.for_date_episodes_title
                }
            })
        }
        onView<RadioButton>(R.id.rb) {
            setChecked(combine(itemChanges, vm.filter) { (current, selected) ->
                when (selected) {
                    is EpisodesFilter.Feed -> {
                        when (selected.state) {
                            FeedState.FUTURE -> current == Filters.FUTURE
                            FeedState.RELEASED -> current == Filters.RELEASED
                        }
                    }
                    is EpisodesFilter.ForDate -> current == Filters.FOR_DATE
                    else -> false
                }
            })
        }
    }

    private fun selectDate(date: Date) {
        val calendar = Calendar.getInstance().apply { time = date }
        DatePickerDialog(
            context,
            { _, year, monthOfYear, dayOfMonth ->
                val result = Calendar.getInstance().apply {
                    set(Calendar.YEAR, year)
                    set(Calendar.MONTH, monthOfYear)
                    set(Calendar.DAY_OF_MONTH, dayOfMonth)
                }

                onFilterApplied(EpisodesFilter.ForDate(result.time))
            },
            calendar.year,
            calendar.month,
            calendar.dayOfMonth
        ).show()
    }

    private fun onFilterApplied(filter: EpisodesFilter) {
        vm.setFilter(filter)
        context.dismissDialogFragment<FilterBottomSheetDialogFragment>()
    }
}