package iam.thevoid.oneom.app.ui.mvvm.router

interface EpisodesRouter {
    fun openSources(episodeId: Long)
}