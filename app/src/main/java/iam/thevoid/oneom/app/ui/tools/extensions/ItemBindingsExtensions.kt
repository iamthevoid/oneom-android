package iam.thevoid.oneom.app.ui.tools.extensions

import android.view.View
import android.view.ViewGroup
import iam.thevoid.noxml.adapterview.ItemBindings
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import kotlin.reflect.KClass

private class StaticLayout<T>(override val layoutRes: Int, parent : ViewGroup) : SplittiesLayout<View, T>(parent)

fun <T : Any> ItemBindings.addBinding(kCls : KClass<T>, layoutRes: Int) =
        addBinding(kCls) { StaticLayout(layoutRes, it) }