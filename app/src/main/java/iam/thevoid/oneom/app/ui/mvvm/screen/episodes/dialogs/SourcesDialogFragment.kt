package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import iam.thevoid.noxml.core.mvvm.parentViewModel
import iam.thevoid.noxml.coroutines.recycler.extensions.setItems
import iam.thevoid.noxml.recycler.setStartSpacing
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.OneOmBottomSheetDialogFragment
import iam.thevoid.oneom.app.ui.shared.OneOmDialogFragment
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.app.ui.tools.sourcesBindings
import splitties.dimensions.dp
import com.google.android.material.bottomsheet.BottomSheetBehavior

import android.widget.FrameLayout

import com.google.android.material.bottomsheet.BottomSheetDialog




class SourcesDialogFragment : OneOmBottomSheetDialogFragment<ConstraintLayout>() {

    companion object {

        private const val EXTRA_EPISODE_ID = "EXTRA_EPISODE_ID"

        fun createInstance(episodeId: Long) =
            SourcesDialogFragment().apply {
                arguments = Bundle().apply {
                    putLong(EXTRA_EPISODE_ID, episodeId)
                }
            }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialog: DialogInterface ->
                (dialog as? BottomSheetDialog)
                    ?.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                    ?.let { BottomSheetBehavior.from(it) }
                    ?.setState(BottomSheetBehavior.STATE_EXPANDED)
            }
        }
    }

    private val vm by parentViewModel<EpisodesViewModel>()

    override fun ConstraintLayout.onRootView() {
        onView<RecyclerView>(R.id.layout_episode_sources_recycler) {
            setStartSpacing(dp(16).toInt())
            isScrollbarFadingEnabled = false
            layoutManager = LinearLayoutManager(context)
            isNestedScrollingEnabled = true
            setItems(vm.sources(arguments?.getLong(EXTRA_EPISODE_ID)), sourcesBindings(vm))
        }
    }

    override val layoutRes: Int
        get() = R.layout.layout_episode_sources
}