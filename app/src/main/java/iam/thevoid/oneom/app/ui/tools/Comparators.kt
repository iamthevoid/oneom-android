package iam.thevoid.oneom.app.ui.tools

import java.util.*
import kotlin.Comparator

object Comparators {
    val dateDescendant by lazy {
        Comparator<Date> { o1, o2 ->
            when  {
                o1 > o2 -> -1
                o2 > o1 -> 1
                else -> 0
            }
        }
    }
}