package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.fragments

import android.os.Bundle
import android.text.format.DateFormat
import android.widget.FrameLayout
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.brandongogetap.stickyheaders.StickyLayoutManager
import iam.thevoid.ae.color
import iam.thevoid.ae.dimen
import iam.thevoid.ae.string
import iam.thevoid.e.safe
import iam.thevoid.noxml.core.mvvm.viewModel
import iam.thevoid.noxml.coroutines.data.CoroutineItem
import iam.thevoid.noxml.coroutines.extensions.toolbar_appcompat.setSubtitle
import iam.thevoid.noxml.coroutines.extensions.toolbar_appcompat.setTitleResource
import iam.thevoid.noxml.coroutines.extensions.view.addSetter
import iam.thevoid.noxml.coroutines.recycler.extensions.setItems
import iam.thevoid.noxml.coroutines.swiperefreshlayout.setOnRefreshListener
import iam.thevoid.noxml.coroutines.swiperefreshlayout.setRefreshing
import iam.thevoid.noxml.recycler.StandaloneRecyclerAdapter
import iam.thevoid.noxml.recycler.resetEndlessScrollState
import iam.thevoid.noxml.recycler.setLoadMore
import iam.thevoid.noxml.recycler.setStartSpacing
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.router.EpisodesRouter
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs.FilterBottomSheetDialogFragment
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs.SourcesDialogFragment
import iam.thevoid.oneom.app.ui.shared.OneOmFragment
import iam.thevoid.oneom.app.ui.tools.episodesBindings
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.app.ui.tools.extensions.showDialogFragment
import iam.thevoid.oneom.data.dataset.episodes.EpisodesFilter
import iam.thevoid.oneom.data.dataset.episodes.FeedState
import kotlinx.coroutines.flow.map
import java.text.SimpleDateFormat

class EpisodesFragment : OneOmFragment<FrameLayout>(R.layout.fragment_episodes), EpisodesRouter {

    private val representationDateFormat = SimpleDateFormat("dd MMMM yyyy")

    private val vm by viewModel<EpisodesViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.router = this
    }

    override fun FrameLayout.onRootView() {

        onView<SwipeRefreshLayout>(R.id.swipe_refresh) {
            setRefreshing(vm.episodesLoader.loading, vm.blockLoading)
            setOnRefreshListener(vm.filter.map {
                SwipeRefreshLayout.OnRefreshListener {
                    if (it is EpisodesFilter.Feed && it.state == FeedState.RELEASED) {
                        vm.refresh()
                    } else {
                        isRefreshing = false
                    }
                }
            })
        }

        onView<Toolbar>(R.id.toolbar) {
            setTitleTextColor(color(R.color.white))
            setTitleResource(vm.filter.map {
                when (it) {
                    is EpisodesFilter.Feed -> {
                        when (it.state) {
                            FeedState.RELEASED -> R.string.released_episodes_title
                            FeedState.FUTURE -> R.string.future_episodes_title
                        }
                    }
                    is EpisodesFilter.ForDate -> R.string.for_date_episodes_title
                }
            })
            setSubtitle(vm.subtitleData.map { (filter, lastUpdate) ->
                when (filter) {
                    is EpisodesFilter.Feed -> {
                        when (filter.state) {
                            FeedState.RELEASED ->
                                DateFormat.getLongDateFormat(context).format(lastUpdate)
                                    .takeIf { lastUpdate.time != 0L }
                                    ?.let { string(R.string.released_episodes_subtitle, it) }
                                    .safe()

                            FeedState.FUTURE ->
                                string(R.string.future_episodes_subtitle)
                        }
                    }

                    is EpisodesFilter.ForDate -> representationDateFormat.format(filter.date)
                }
            })
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.filter -> showFilterPopup()
                }
                false
            }
        }

        onView<RecyclerView>(R.id.recycler) {
            layoutManager = StickyLayoutManager(context) {
                (adapter as? StandaloneRecyclerAdapter<Any>)?.data
            }
            triggerEndlessScroll(vm.scrollStateTrigger)
            setItems(vm.episodes, episodesBindings(vm))
            setStartSpacing(dimen(R.dimen.side_spacing_v_half))
            setLoadMore { vm.episodesLoader.loadMore() }
        }
    }

    override fun openSources(episodeId: Long) {
        showDialogFragment(SourcesDialogFragment.createInstance(episodeId))
    }

    private fun showFilterPopup() = showDialogFragment(FilterBottomSheetDialogFragment())

    private fun RecyclerView.triggerEndlessScroll(trigger: CoroutineItem<Any>) =
        addSetter(trigger.observe()) { resetEndlessScrollState() }
}