package iam.thevoid.oneom.app.ui.mvvm.screen.splash

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import iam.thevoid.ae.string
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.Router
import iam.thevoid.oneom.app.ui.shared.OneOmFragment
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import org.koin.android.ext.android.inject

class SplashFragment : OneOmFragment<FrameLayout>(R.layout.fragment_splash) {

    private val vm by inject<SplashViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.router = (activity as? Router)
    }

    override fun FrameLayout.onRootView() {
        onView<TextView>(R.id.text_version) {
            text = string(R.string.version, vm.version)
        }
    }
}