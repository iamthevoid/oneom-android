package iam.thevoid.oneom.app.ui.mvvm.screen.episodes

import androidx.lifecycle.ViewModel
import com.brandongogetap.stickyheaders.exposed.StickyHeader
import iam.thevoid.e.safe
import iam.thevoid.noxml.coroutines.data.CoroutineBoolean
import iam.thevoid.noxml.coroutines.data.CoroutineItem
import iam.thevoid.noxml.coroutines.data.CoroutineLong
import iam.thevoid.noxml.recycler.paging.PageLoader
import iam.thevoid.oneom.app.ui.mvvm.router.EpisodesRouter
import iam.thevoid.oneom.app.util.Time
import iam.thevoid.oneom.data.dataset.episodes.EpisodesArchive
import iam.thevoid.oneom.data.dataset.episodes.EpisodesFilter
import iam.thevoid.oneom.data.dataset.episodes.FeedState
import iam.thevoid.oneom.data.model.Episode
import iam.thevoid.util.weak
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject
import java.util.*
import kotlin.coroutines.EmptyCoroutineContext

class EpisodesViewModel : ViewModel() {

    private val archive: EpisodesArchive by inject(EpisodesArchive::class.java)

    private val lastUpdate = CoroutineLong(0)

    var router by weak<EpisodesRouter>()

    val scrollStateTrigger = CoroutineItem(Any())

    val blockLoading = CoroutineBoolean()

    val episodesLoader = archive.episodesFeed()

    val filter = archive.episodesFilter().distinctUntilChanged()

    val episodes = filter
        .onEach { scrollStateTrigger.set(Any()) }
        .flatMapLatest {
            when (it) {
                is EpisodesFilter.Feed -> {
                    when (it.state) {
                        FeedState.RELEASED -> released()
                        FeedState.FUTURE -> future()
                    }
                }
                is EpisodesFilter.ForDate -> forDate(it.date)
            }
        }
        .onEach { lastUpdate.set(System.currentTimeMillis()) }

    val subtitleData: Flow<SubtitleData> =
        combine(filter, lastUpdate.observe()) { filter, lastUpdateMillis ->
            SubtitleData(filter, Date(lastUpdateMillis))
        }

    fun refresh() {
        // TODO LEAK!!!
        // TODO REFRESH OTHERS
        CoroutineScope(EmptyCoroutineContext).launch {
            when (val filter = filter.last()) {
                is EpisodesFilter.Feed -> {
                    if (filter.state == FeedState.RELEASED) episodesLoader.refresh() else {
                        blockLoading.set(false)
                    }
                }
                else -> blockLoading.set(false)
            }
        }
    }

    fun serialFor(episode: Episode) = archive.serialFor(episode)

    fun posterFor(episode: Episode) = archive.posterFor(episode)

    fun setFilter(filter: EpisodesFilter) {
        archive.changeEpisodesFilter(filter)
    }

    fun sources(episodeId: Long?): Flow<List<Any>> {
        return episodeId?.let(archive::episode)
            ?.map { episode ->
                mutableListOf<Any>().apply {
                    episode.torrent.takeIf { it.isNotEmpty() }?.also {
                        add(SourceHeader.Torrent)
                        addAll(it)
                    }
                    episode.online.takeIf { it.isNotEmpty() }?.also {
                        add(SourceHeader.Online)
                        addAll(it)
                    }
                    episode.subtitle.takeIf { it.isNotEmpty() }?.also {
                        add(SourceHeader.Subtitle)
                        addAll(it)
                    }
                }
            } ?: flowOf(emptyList())
    }

    fun onSourcesClick(episode: Episode?) {
        episode?.id?.also { router?.openSources(it) }
    }

    private fun future(): Flow<List<Any>> = archive.futureEpisodes()
        .onStart { blockLoading.set(true) }
        .flowOn(Dispatchers.IO)
        .map { episodes -> episodes.groupBy { it.airDate }.mapKeys { it.key.time } }
        .map { episodeGroups ->
            mutableListOf<Any>().apply {
                episodeGroups.entries.filter { it.value.isNotEmpty() }
                    .sortedBy { it.key }
                    .forEach {
                        add(Header(it.value.first().airDate))
                        addAll(it.value.group())
                    }
            }
        }
        .onEach { blockLoading.set(false) }
        .onCompletion { blockLoading.set(false) }

    fun forDate(date: Date) =
        archive.episodesForDate(date)
            .onStart { blockLoading.set(true) }
            .flowOn(Dispatchers.IO)
            .map { episodes -> episodes.groupBy { it.airDate }.mapKeys { it.key.time } }
            .map { episodeGroups ->
                mutableListOf<Any>().apply {
                    episodeGroups.entries.filter { it.value.isNotEmpty() }
                        .sortedBy { it.key }
                        .forEach {
                            add(Header(it.value.first().airDate))
                            addAll(it.value.group())
                        }
                }
            }
            .onEach { blockLoading.set(false) }

    private fun released() = episodesLoader.pages
        .flowOn(Dispatchers.IO)
        .map { pages -> configurePages(pages) }

    private fun configurePages(pages: List<PageLoader.Page<Int, Episode>>) =
        mutableListOf<Any>().apply {
            pages.sortedBy { it.pageIndex }
                .forEach { page ->
                    val date = Date(Date().time - page.pageIndex * Time.DAY)
                    val episodes = page.items

                    add(Header(date))

                    if (episodes.isEmpty())
                        add(Stub)

                    addAll(episodes.group())
                }

            add(Spacing)

        }

    private fun List<Episode>.group(): List<Any> = groupBy { it.serialTitle }
        .map {
            if (it.value.size == 1) {
                it.value.first()
            } else {
                Stack(with(it.value) {
                    mapIndexed { index, episode ->
                        StackEpisode(episode, index == lastIndex)
                    }
                })
            }
        }.sortedByDescending { item ->
            when (item) {
                is Episode -> item.updatedAt
                is Stack -> item.episodes.map { it.episode.updatedAt }.maxOrNull().safe()
                else -> throw IllegalArgumentException("${item::class.java.simpleName} in episodes list")
            }
        }

    data class SubtitleData(val filter: EpisodesFilter, val lastUpdate: Date)

    data class Header(val date: Date) : StickyHeader

    data class Stack constructor(val episodes: List<StackEpisode>)

    data class StackEpisode(val episode: Episode, val isLast: Boolean)

    object Stub

    object Spacing

    enum class SourceHeader {
        Torrent,
        Online,
        Subtitle
    }
}