package iam.thevoid.oneom.app.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import iam.thevoid.ae.gone
import iam.thevoid.noxml.coroutines.extensions.view.addSetter
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.tools.extensions.disabled
import iam.thevoid.oneom.data.model.Episode
import kotlinx.coroutines.flow.Flow

class SourceCounter @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attributeSet, defStyleAttr, defStyleRes) {

    private var subtitleCountText: TextView? = null
    private var subtitleIcon: View? = null
    private var onlineCountText: TextView? = null
    private var onlineIcon: View? = null
    private var torrentCountText: TextView? = null
    private var torrentIcon: View? = null

    init {
        inflate(context, R.layout.view_source_counter, this)
        subtitleCountText = findViewById(R.id.text_count_subtitles)
        subtitleIcon = findViewById(R.id.icon_subtitles)
        torrentCountText = findViewById(R.id.text_count_torrents)
        torrentIcon = findViewById(R.id.icon_torrents)
        onlineCountText = findViewById(R.id.text_count_online)
        onlineIcon = findViewById(R.id.icon_online)
    }

    var subtitleCount: Int = 0
        set(count) {
            field = count
            updateLayout()
        }

    var onlineCount: Int = 0
        set(count) {
            field = count
            updateLayout()
        }

    var torrentCount: Int = 0
        set(count) {
            field = count
            updateLayout()
        }

    private fun updateLayout() {

        val isNoSubtitles = subtitleCount <= 0

        subtitleCountText?.text = "$subtitleCount"
        subtitleCountText?.disabled(isNoSubtitles)
        subtitleIcon?.disabled(isNoSubtitles)
        subtitleCountText.gone(isNoSubtitles)
        subtitleIcon.gone(isNoSubtitles)

        val isNoOnlines = onlineCount <= 0
        val isOnlinesGone = isNoSubtitles && isNoOnlines

        onlineCountText?.text = "$onlineCount"
        onlineCountText.gone(isOnlinesGone)
        onlineIcon.gone(isOnlinesGone)
        onlineCountText?.disabled(isNoOnlines)
        onlineIcon?.disabled(isNoOnlines)

        val isNoTorrents = torrentCount <= 0
        val isTorrentsGone = isOnlinesGone && isNoTorrents

        torrentCountText?.text = "$torrentCount"
        torrentCountText.gone(isTorrentsGone)
        torrentIcon.gone(isTorrentsGone)
        torrentCountText?.disabled(isNoTorrents)
        torrentIcon?.disabled(isNoTorrents)
    }
}

fun SourceCounter.setCount(episode: Flow<Episode>) =
    addSetter(episode) {
        subtitleCount = it.subtitleCount
        onlineCount = it.onlineCount
        torrentCount = it.torrentCount
    }