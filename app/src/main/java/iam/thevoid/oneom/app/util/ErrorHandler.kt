package iam.thevoid.oneom.app.util

import java.lang.IllegalStateException
import java.util.*

object ErrorHandler {

    private const val TIME = "___________time"
    private const val THREAD = "____thread_name"
    private const val DEVICE_BRAND = "___device_brand"
    private const val DEVICE_CPU = "_____device_cpu"
    private const val DEVICE_TYPE = "____device_type"
    private const val DEVICE_DISPLAY = "_device_display"
    private const val DEVICE_MODEL = "___device_model"
    private const val DEVICE_SDK = "_____device_sdk"
    private const val DEVICE_RAM = "_____device_ram"
    private const val DEVICE_OS = "______device_os"
    private const val STACKTRACE_FORMAT = "stack_trace_%03d"
    private const val CAUSED_BY = "Caused by: "
    private const val AT = "\t\tat "
    private const val MB_RAM = "MB RAM"
    private const val EMPTY = ""

    private var i = 0

    private val deviceDataPairs: Map<String, String>
        get() {
            val data = TreeMap<String, String>()

            val device = Device

            data[DEVICE_BRAND] = device.BRAND
            data[DEVICE_CPU] = device.CPU.joinToString()
            data[DEVICE_TYPE] = device.DEVICE_TYPE
            data[DEVICE_DISPLAY] = device.DISPLAY_INFO
            data[DEVICE_MODEL] = device.MODEL
            data[DEVICE_SDK] = "${device.SDK}"
            data[DEVICE_RAM] = device.RAM_MEGABYTES.toString() + MB_RAM
            data[DEVICE_OS] = device.OS_VERSION

            return data
        }

    fun handleError(thread: Thread?, exception: Throwable?, log: Boolean) {

        thread ?: throw IllegalStateException("Error on no tread")

        exception ?: throw IllegalArgumentException("Caught no error")

        val data = collectErrorData(thread, exception)
        if (log) {
            for ((key, value) in data) { println("$key $value") }
        }
//        GlobalScope.async { Web.reportError(data).await() }
    }

    private fun collectErrorData(thread: Thread, exception: Throwable): Map<String, String> {
        val data = TreeMap<String, String>()

        data.putAll(deviceDataPairs)
        data.putAll(getMainDataMap(thread))
        data.putAll(getCauseStackTraceMap(exception))

        return data

    }

    private fun getMainDataMap(thread: Thread): Map<String, String> {
        val data = TreeMap<String, String>()

//        data[BUILD_TYPE] = BuildConfig.BUILD_TYPE
        data[TIME] = Date(System.currentTimeMillis()).toString()
        data[THREAD] = thread.name

        return data
    }

    private fun getCauseStackTraceMap(e: Throwable?): Map<String, String> {
        val data = TreeMap<String, String>()

        if (e == null) {
            return data
        }

        data[String.format(Locale.ENGLISH, STACKTRACE_FORMAT, i++)] = (if (i == 1) EMPTY else CAUSED_BY) + e.toString()
        data.putAll(getThrowableStackTraceMap(e))
        data.putAll(getCauseStackTraceMap(e.cause))

        return data
    }

    private fun getThrowableStackTraceMap(e: Throwable?): Map<String, String> {
        val data = TreeMap<String, String>()

        if (e == null) {
            return data
        }

        for (s in e.stackTrace) {
            data[String.format(Locale.ENGLISH, STACKTRACE_FORMAT, i++)] = AT + s.toString()
        }

        return data
    }

    class ErrorHandlerDelegate(private val delegate : Thread.UncaughtExceptionHandler?) : Thread.UncaughtExceptionHandler {
        override fun uncaughtException(p0: Thread?, p1: Throwable?) {
            handleError(p0, p1, true)
            p0 ?: return
            p1 ?: return
            delegate?.uncaughtException(p0, p1)
        }
    }

}
