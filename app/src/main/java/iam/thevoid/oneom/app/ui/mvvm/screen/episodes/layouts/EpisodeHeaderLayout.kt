package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.text.format.DateFormat
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import iam.thevoid.noxml.coroutines.extensions.textview.setText
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import kotlinx.coroutines.flow.map

class EpisodeHeaderLayout(parent: ViewGroup) : SplittiesLayout<FrameLayout, EpisodesViewModel.Header>(parent) {

    override val layoutRes: Int
        get() = R.layout.layout_episode_header

    override fun FrameLayout.onRootView() {
        onView<TextView>(R.id.text_header) {
            setText(itemChanges.map { DateFormat.getLongDateFormat(context).format(it.date) })
        }
    }
}