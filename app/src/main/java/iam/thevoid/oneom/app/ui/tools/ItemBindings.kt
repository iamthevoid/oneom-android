package iam.thevoid.oneom.app.ui.tools

import iam.thevoid.noxml.adapterview.ItemBindings
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.dialogs.Filters
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts.*
import iam.thevoid.oneom.app.ui.tools.extensions.addBinding
import iam.thevoid.oneom.data.dataset.episodes.EpisodesFilter
import iam.thevoid.oneom.data.model.Episode
import iam.thevoid.oneom.data.model.Online
import iam.thevoid.oneom.data.model.Subtitle
import iam.thevoid.oneom.data.model.Torrent

fun episodeFiltersBindings(vm: EpisodesViewModel): ItemBindings =
    ItemBindings.of(Filters::class) { EpisodeFilterLayout(vm, it) }

fun episodesBindings(vm: EpisodesViewModel): ItemBindings =
    ItemBindings.of(Episode::class) { EpisodeLayout(vm, it) }
        .addBinding(EpisodesViewModel.Stack::class) { EpisodesStackLayout(vm, it) }
        .addBinding(EpisodesViewModel.Header::class, ::EpisodeHeaderLayout)
        .addBinding(EpisodesViewModel.Stub::class, R.layout.layout_episode_stub)
        .addBinding(EpisodesViewModel.Spacing::class, R.layout.layout_spacing_quad)

fun sourcesBindings(vm: EpisodesViewModel): ItemBindings =
    ItemBindings.of(Torrent::class) { TorrentDialogLayout(vm, it) }
        .addBinding(EpisodesViewModel.SourceHeader::class, ::HeaderDialogLayout)
        .addBinding(Online::class, R.layout.layout_episode_stub)
        .addBinding(Subtitle::class, R.layout.layout_spacing_quad)

fun episodesStackBindings(vm: EpisodesViewModel): ItemBindings =
    ItemBindings.of(EpisodesViewModel.StackEpisode::class) { StackEpisodeLayout(vm, it) }