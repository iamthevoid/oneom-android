@file:Suppress("unused")

package iam.thevoid.oneom.app.util

import iam.thevoid.oneom.data.model.Episode
import iam.thevoid.oneom.data.model.Serial


object OneOmUtil {

    // Returns formatted episode in season number EQ: S03E24
    fun episodeInSeasonString(episode: Episode): String {
        val s = episode.season
        val e = episode.ep

        if (s == 0 && e == 0) {
            return ""
        }

        if (s == 0) {
            return String.format("E%02d", e)
        }

        return if (e == 0) {
            String.format("S%02d", s)
        } else String.format("S%02dE%02d", s, e)

    }

//    fun setSchedule(episodes: List<Episode>): List<Episode> {
//        for (episode in episodes) {
//            episode.setIsSheldule(true)
//        }
//
//        return episodes
//    }

//    fun period(context: Context, serial: Serial): String {
//
//        val start = serial.start
//        val end = serial.end
//
//        if (start == null) {
//            return context.getString(R.string.unknown)
//        }
//
//        var addition = ""
//
//        try {
//            if (end == null || end == 0) {
//                addition = ""
//            } else if (Time.getYear(Date(end)).time < 0) {
//                addition = ""
//            } else if (Time.getYear(Date(start)) == Time.getYear(Date(end))) {
//                addition = ""
//            } else if (Date(end).after(Date())) {
//                addition = " - " + context.getString(R.string.till_now)
//            } else {
//                addition = " - " + Time.format(Date(end), Time.TimeFormat.YEAR)
//            }
//        } catch (e: ParseException) {
//            e.printStackTrace()
//        }
//
//        return Time.format(Date(start), Time.TimeFormat.YEAR) + addition
//    }

//    fun countries(serial: Serial): String {
//        if (serial.country == null || serial.country.size == 0) {
//            return ""
//        }
//
//        val stringBuilder = StringBuilder()
//        var i = 0
//        val l = serial.country.size
//        while (i < l) {
//            stringBuilder.append(serial.country[i].name)
//            if (i != l - 1) {
//                stringBuilder.append(", ")
//            }
//            i++
//        }
//        return stringBuilder.toString()
//    }

//    fun networks(serial: Serial): String {
//        if (serial.network == null || serial.network.size == 0) {
//            return ""
//        }
//
//        val stringBuilder = StringBuilder()
//        var i = 0
//        val l = serial.network.size
//        while (i < l) {
//            stringBuilder.append(serial.network[i].name)
//            if (i != l - 1) {
//                stringBuilder.append(", ")
//            }
//            i++
//        }
//        return stringBuilder.toString()
//    }

//    fun title(episode: Episode?): String {
//        return if (episode == null || episode.serial == null) {
//            ""
//        } else title(episode.serial)
//
//    }

    fun title(serial: Serial): String {
        return serial.title
    }

//    fun posterUrl(episode: Episode?, resolution: String): String {
//        return if (episode == null || episode.serial == null) {
//            ""
//        } else posterUrl(episode.serial, resolution)
//
//    }

//    fun posterUrl(serial: Serial?, resolution: String): String {
//
//        if (serial == null) {
//            return ""
//        }
//
//        val poster = serial.poster ?: return ""
//
//
//        return Web.poster_prefix + poster.path + "/" + resolution + poster.name
//    }

//    fun posterTint(episode: Episode?): Int {
//        return if (episode == null || episode.serial == null) {
//            -0x1
//        } else posterTint(episode.serial)
//
//    }

//    fun posterTint(serial: Serial?): Int {
//
//        if (serial == null) {
//            return -0x1
//        }
//
//        return if (serial.poster == null) {
//            -0x1
//        } else serial.poster.tintColor
//
//    }

//    fun storePosterTint(episode: Episode?, tintColor: Int) {
//        if (episode == null || episode.serial == null) {
//            return
//        }
//
//        storePosterTint(episode.serial, tintColor)
//    }

//    fun storePosterTint(serial: Serial?, tintColor: Int) {
//        Log.d("fd", "storePosterTint: ")
//        if (serial == null) {
//            return
//        }
//
//        if (serial.poster == null) {
//            return
//        }
//
//        //        Serial s = DbHelper.clone(serial);
//
//        //        s.getPoster().setTintColor(tintColor);
//
//        //        DbHelper.insert(serial);
//    }


//    fun serialStatusName(serial: Serial?): String {
//        return if (serial == null || serial.status == null) {
//            "Unknown"
//        } else serial.status.name
//    }

//    fun episodesForSeasonList(serial: Serial, seasonNumber: Int): List<Episode> {
//        val res = ArrayList<Episode>()
//
//        for (e in serial.episode) {
//            if (e.season == seasonNumber) res.add(e)
//        }
//        return res
//    }

//    fun seasonsCountForSerial(serial: Serial): Int {
//        var epCount = 0
//
//        if (serial.episode == null) {
//            return 0
//        }
//
//        for (episode in serial.episode) {
//
//            if (episode.season == 0) {
//                return 0
//            }
//
//            val i = episode.season
//
//            if (i <= epCount) {
//                continue
//            }
//
//            if (i > epCount) {
//                epCount = i
//            }
//        }
//
//        return epCount
//    }

//    fun episodesCountForSeason(serial: Serial, selected: Int): Int {
//        return episodesForSeasonList(serial, selected).size
//    }

//    fun sourceNames(sources: List<Source>): List<String> {
//        val names = ArrayList<String>()
//        for (source in sources) {
//            names.add(source.name)
//        }
//        return names
//    }

//    fun langNames(langs: List<Lang>): List<String> {
//        val names = ArrayList<String>()
//        for (lang in langs) {
//            if (lang.name != null) names.add(lang.name)
//        }
//        return names
//    }

//    fun langShortNames(langs: List<Lang>): List<String> {
//        val names = ArrayList<String>()
//        for (lang in langs) {
//            if (lang.shortName != null) names.add(lang.shortName)
//        }
//        return names
//    }

//    fun qgNames(qualityGroups: List<QualityGroup>): List<String> {
//        val names = ArrayList<String>()
//        for (qualityGroup in qualityGroups) {
//            names.add(qualityGroup.name)
//        }
//        return names
//    }

//    fun description(episode: Episode, defaultText: String?): CharSequence {
//        return if (episode.description == null || episode.description.size == 0) {
//            if (defaultText == null) "" else Html.fromHtml(defaultText)
//        } else Html.fromHtml(episode.description[0].body)
//
//    }

//    fun isEmptySource(source: Source): Boolean {
//        return source.searchPage == null || source.searchPage.length == 0
//    }

//    fun hasOnlines(episode: Episode): Boolean {
//        return episode.online != null && episode.online.size > 0
//    }

//    fun onlinesCount(episode: Episode): Int {
//        return if (episode.online == null) 0 else episode.online.size
//    }

//    fun hasTorrents(episode: Episode): Boolean {
//        return episode.torrent != null && episode.torrent.size > 0
//    }

//    fun torrentsCount(episode: Episode): Int {
//        return if (episode.torrent == null) 0 else episode.torrent.size
//    }

//    fun searchString(episode: Episode): String {
//        return episode.serial.title
//    }

//    fun episodeRelatedCount(episode: Episode): Int {
//        var count = 0
//        if (episode.online != null) {
//            count += episode.online.size
//        }
//        if (episode.torrent != null) {
//            count += episode.torrent.size
//        }
//        return count
//    }
}
