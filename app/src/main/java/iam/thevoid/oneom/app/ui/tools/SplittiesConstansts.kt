package iam.thevoid.oneom.app.ui.tools

import android.view.View
import iam.thevoid.oneom.R
import splitties.dimensions.dip

val View.halfSideSpacing
    get() = dip(8)

val View.sideSpacing
    get() = dip(16)

val View.doubleSideSpacing
    get() = dip(32)

val View.bigIconSize
    get() = dip(32)

val normalTextSizeResource
    get() = R.dimen.text_normal