package iam.thevoid.oneom.app.ui.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import iam.thevoid.ae.inflate

abstract class OneOmBottomSheetDialogFragment<Root : View> : BottomSheetDialogFragment() {

    @get:LayoutRes
    abstract val layoutRes : Int

    open fun Root.onRootView() = Unit

    private var contentView : View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            (contentView ?: context?.inflate(layoutRes, container, false)
                    .also { contentView = it })

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as? Root)?.onRootView()
    }
}