@file:OptIn(ExperimentalCoroutinesApi::class)

package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import iam.thevoid.e.safe
import iam.thevoid.noxml.coroutines.extensions.textview.setText
import iam.thevoid.noxml.coroutines.extensions.view.setOnClickListener
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.app.ui.tools.extensions.setPoster
import iam.thevoid.oneom.app.ui.tools.extensions.shareText
import iam.thevoid.oneom.app.ui.tools.shareTextIntent
import iam.thevoid.oneom.app.ui.view.SourceCounter
import iam.thevoid.oneom.app.ui.view.setCount
import iam.thevoid.oneom.data.model.Episode
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map

class EpisodeLayout(private val vm: EpisodesViewModel, parent: ViewGroup) :
    SplittiesLayout<CardView, Episode>(parent) {

    override val layoutRes: Int
        get() = R.layout.layout_episode

    override fun CardView.onRootView() {

        onView<TextView>(R.id.text_title) {
            setText(itemChanges.map { it.serialTitle })
        }

        onView<TextView>(R.id.text_subtitle) {
            setText(itemChanges.map { "${it.se()}: ${it.title}" })
        }

        onView<ImageView>(R.id.image_episode_poster) {
            setPoster(itemChanges.flatMapLatest { vm.posterFor(it) }.map { it?.url().safe() })
        }

        onView<SourceCounter>(R.id.button_related_source) {
            setCount(itemChanges)
        }

        onView<ImageView>(R.id.icon_share) {
            setOnClickListener(itemChanges.flatMapLatest { episode ->
                vm.posterFor(episode).map {
                    episode to it
                }.map { (episode, poster) ->
                    View.OnClickListener {
                        DateFormat.getLongDateFormat(context)
                            .let { episode.shareText(it, poster) }
                            .let(::shareTextIntent)
                            .also(context::startActivity)
                    }
                }
            })
        }

        onView<View>(R.id.button_related_source) {
            setOnClickListener(itemChanges.map { episode ->
                View.OnClickListener {
                    vm.onSourcesClick(episode)
                }
            })
        }
    }
}