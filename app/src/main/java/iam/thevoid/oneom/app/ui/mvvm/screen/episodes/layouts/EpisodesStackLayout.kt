@file:OptIn(ExperimentalCoroutinesApi::class)

package iam.thevoid.oneom.app.ui.mvvm.screen.episodes.layouts

import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import iam.thevoid.ae.quantityString
import iam.thevoid.e.safe
import iam.thevoid.noxml.coroutines.extensions.textview.setText
import iam.thevoid.noxml.coroutines.extensions.view.setOnClickListener
import iam.thevoid.noxml.coroutines.recycler.extensions.setItems
import iam.thevoid.oneom.R
import iam.thevoid.oneom.app.ui.mvvm.screen.episodes.EpisodesViewModel
import iam.thevoid.oneom.app.ui.shared.SplittiesLayout
import iam.thevoid.oneom.app.ui.tools.episodesStackBindings
import iam.thevoid.oneom.app.ui.tools.extensions.onView
import iam.thevoid.oneom.app.ui.tools.extensions.setPoster
import iam.thevoid.oneom.app.ui.tools.extensions.shareText
import iam.thevoid.oneom.app.ui.tools.shareTextIntent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map

class EpisodesStackLayout(private val vm: EpisodesViewModel, parent: ViewGroup) :
    SplittiesLayout<CardView, EpisodesViewModel.Stack>(parent) {

    override val layoutRes: Int
        get() = R.layout.layout_episode_stack

    override fun CardView.onRootView() {

        onView<RecyclerView>(R.id.recycler) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(false)
            isNestedScrollingEnabled = false
            setItems(itemChanges.map { it.episodes }, episodesStackBindings(vm))
        }

        onView<TextView>(R.id.text_title) {
            setText(itemChanges.flatMapLatest { vm.serialFor(it.episodes.first().episode) }
                .map { it.title })
        }

        onView<ImageView>(R.id.icon_share) {
            setOnClickListener(
                itemChanges.flatMapLatest { stack ->
                    vm.posterFor(stack.episodes.first().episode).map {
                        stack to it
                    }.map { (stack, poster) ->
                        View.OnClickListener {
                            DateFormat.getLongDateFormat(context)
                                .let { stack.shareText(it, poster) }
                                .let(::shareTextIntent)
                                .also(context::startActivity)
                        }
                    }
                }
            )
        }

        onView<TextView>(R.id.text_subtitle) {
            setText(itemChanges.map {
                context.quantityString(
                    R.plurals.episodes,
                    it.episodes.size
                )
            })
        }

        onView<ImageView>(R.id.image_episode_poster) {
            setPoster(itemChanges.flatMapLatest { vm.posterFor(it.episodes.first().episode) }
                .map { it?.url().safe() })
        }
    }
}