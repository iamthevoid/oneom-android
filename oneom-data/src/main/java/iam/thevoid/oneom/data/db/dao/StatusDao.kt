package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.LangDO
import iam.thevoid.oneom.data.db.model.StatusDO

@androidx.room.Dao
internal abstract class StatusDao : BaseDao<StatusDO>() {

    @Query("SELECT COUNT(*) FROM StatusDO")
    internal abstract suspend fun count(): Long

    @Query("SELECT * FROM StatusDO WHERE id = :id")
    internal abstract suspend fun byId(id : Long): StatusDO

}