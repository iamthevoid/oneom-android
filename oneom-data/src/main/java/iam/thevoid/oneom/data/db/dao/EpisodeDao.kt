package iam.thevoid.oneom.data.db.dao

import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import iam.thevoid.e.parseSafe
import iam.thevoid.e.unixTime
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.EpisodeDO
import iam.thevoid.oneom.data.util.Time.DAY
import iam.thevoid.oneom.data.util.Time.UNIXEPOCH_DAY
import kotlinx.coroutines.flow.Flow
import java.util.*

@Suppress("FunctionName")

@androidx.room.Dao
internal abstract class EpisodeDao : BaseDao<EpisodeDO>() {

    @Query("SELECT COUNT(*) FROM EpisodeDO")
    abstract suspend fun count(): Long

    @Query("SELECT * FROM EpisodeDO WHERE id = :id")
    abstract suspend fun episode(id : Long): EpisodeDO

    @Query("SELECT * FROM EpisodeDO WHERE airdate in (:airdate)")
    abstract suspend fun byAirdate(airdate: String): List<EpisodeDO>

    @Query("SELECT CAST (STRFTIME('%s', ed.airdate) AS INTEGER) ts, * FROM  EpisodeDO ed WHERE ts >= :timestamp")
    abstract fun fromTimestamp(timestamp : Long): Flow<List<EpisodeDO>>

    @Query("SELECT * FROM EpisodeDO WHERE airdate <= :to AND airdate >= :from ORDER BY airdate DESC")
    abstract fun _episodesFromTo(from: Long, to: Long): Flow<List<EpisodeDO>>

    @Query("SELECT * FROM EpisodeDO WHERE airdate = :dateRepresentation")
    abstract fun forDate(dateRepresentation: String): Flow<List<EpisodeDO>>

    @Query("SELECT * FROM EpisodeDO WHERE serialId = :serialId ORDER BY airdate DESC")
    abstract suspend fun _episodesForSerial(serialId: Long): List<EpisodeDO>

    @Query("DELETE FROM EpisodeDO")
    abstract suspend fun deleteAll()

    fun future() =
            fromTimestamp(EpisodeDO.AIRDATE_DATE_FORMAT.run { parseSafe(format(Date())).unixTime } + UNIXEPOCH_DAY)
}