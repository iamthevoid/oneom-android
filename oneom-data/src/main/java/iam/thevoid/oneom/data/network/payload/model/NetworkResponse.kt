package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.NETWORK_COUNTRY_ID
import iam.thevoid.oneom.data.NETWORK_ID
import iam.thevoid.oneom.data.NETWORK_NAME
import iam.thevoid.oneom.data.NETWORK_WEIGHT
import iam.thevoid.oneom.data.db.model.NetworkDO

/*
{
"id": 1087,
"country_id": 0,
"weight": 0,
"name": "Young Animal Densi"
}
*/

internal data class NetworkResponse(

        @SerializedName(NETWORK_ID)
        var id: Long = 0,

        @SerializedName(NETWORK_NAME)
        var name: String = "",

        @SerializedName(NETWORK_WEIGHT)
        var weight: Float = 0f,

        @SerializedName(NETWORK_COUNTRY_ID)
        var countryId: Long = 0
)

internal fun NetworkResponse.toDO() =
        NetworkDO(id, name, weight, countryId)

internal fun List<NetworkResponse>.toDO() =
        map { it.toDO() }