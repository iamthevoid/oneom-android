package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class VariantDO(

        @PrimaryKey
        var id: Long = 0,
        var serialId: Long = 0,
        var title: String = "",
        var start: Long = 0,
        var end: Long = 0,
        var updatedAt: Long = 0
)