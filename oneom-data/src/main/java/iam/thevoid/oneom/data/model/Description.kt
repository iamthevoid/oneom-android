package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.DescriptionDO

data class Description(private val description: DescriptionDO) {
    val text: String
        get() = description.body
}