package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.OnlineDO

data class Online(
    private val online: OnlineDO,
    val lang: Lang,
    val quality: Quality,
    val source: Source
) {

    val title: String
        get() = online.title

    val url: String
        get() = online.url

    val videoUrl: String
        get() = online.videoUrl

    val trust: Boolean
        get() = online.trust

    val embedCode: String
        get() = online.embedCode



    internal val id: Long
        get() = online.id

    internal val langId: Long
        get() = online.langId

    internal val sourceId: Long
        get() = online.sourceId

    internal val qualityId: Long
        get() = online.qualityId

    internal val updatedAt: Long
        get() = online.updatedAt

    internal val deletedAt: Long
        get() = online.deletedAt
}