package iam.thevoid.oneom.data.network.payload.request

class UserLoginRequest(val login: String, val password: String)