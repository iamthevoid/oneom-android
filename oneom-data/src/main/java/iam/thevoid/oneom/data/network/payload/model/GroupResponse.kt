package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.GroupDO

/*
{
"id": 1,
"name": "NovaFilm",
"type_id": 1,
"weight": 0,
"from_lang_id": 1,
"to_lang_id": 2,
"avatar_id": 0,
"url": "http:\/\/novafilm.tv\/",
"created_at": null,
"updated_at": null,
"deleted_at": null
}
*/

internal data class GroupResponse(

        @SerializedName(GROUP_ID)
        var id: Long = 0,

        @SerializedName(GROUP_NAME)
        var name: String? = null,

        @SerializedName(GROUP_TYPE_ID)
        var typeId: Long = 0,

        @SerializedName(GROUP_WEIGHT)
        var weight: Double = .0,

        @SerializedName(GROUP_FROM_LANG_ID)
        var fromLangId: Long = 0,

        @SerializedName(GROUP_TO_LANG_ID)
        var toLangId: Long = 0,

        @SerializedName(GROUP_AVATAR_ID)
        var avatarId: Long = 0,

        @SerializedName(GROUP_URL)
        var url: String? = null

)

internal fun GroupResponse.toDO() =
        GroupDO(id, name, typeId, weight, fromLangId, toLangId, avatarId, url)

internal fun List<GroupResponse>.toDO() =
        map { it.toDO() }