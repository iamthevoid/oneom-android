package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.POSTER_ALT
import iam.thevoid.oneom.data.POSTER_ID
import iam.thevoid.oneom.data.POSTER_NAME
import iam.thevoid.oneom.data.POSTER_PATH
import iam.thevoid.oneom.data.db.model.OnlineDO
import iam.thevoid.oneom.data.db.model.PosterDO

/*
{
"id": 95,
"name": "100.png",
"alt": "The Killing Season",
"path": "serial"
}
*/

internal data class PosterResponse (

        @SerializedName(POSTER_ID)
        var id : Long = 0,

        @SerializedName(POSTER_NAME)
        var name : String = "",

        @SerializedName(POSTER_ALT)
        var alt : String = "",

        @SerializedName(POSTER_PATH)
        var path : String = ""
)

internal fun PosterResponse.toDO() =
        PosterDO(id, name, alt, path)

internal fun List<PosterResponse>.toDO() =
        map { it.toDO() }