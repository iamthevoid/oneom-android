package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.DescriptionDO
import iam.thevoid.oneom.data.db.model.OnlineDO
import iam.thevoid.oneom.data.network.jsonadapter.TimestampLongAdapter

/*
{
  "id": 101949,
  "assoc_id": "34260",
  "assoc_type": "App\\Serial",
  "type_id": "1",
  "body": "<p>Disney's <b>The Wuzzles</b> was an American animated television series created for Saturday morning television, and was first broadcast on September 14, 1985 on CBS. The Wuzzles features a variety of short, rounded animal characters (each called a Wuzzle, which means to mix up). Each is a roughly even, and colorful, mix of two different animal species (as the theme song mentions, \"...livin' with a split personality\"), and all the characters sport wings on their backs, although only Bumblelion and Butterbear are seemingly capable of flight. All of the Wuzzles live on the Isle of Wuz. Double species are not limited to the Wuzzles themselves. From the appleberries they eat to the telephonograph in the home, or a luxury home called a castlescraper, nearly everything on Wuz is mixed together in the same way the Wuzzles are. The characters in the show were marketed extensively.</p>",
  "lang_id": "1",
  "created_at": "2016-08-21 15:05:31",
  "updated_at": "2017-12-17 02:20:17",
  "deleted_at": null
}
{
  "id": 197993,
  "assoc_id": "1768161",
  "assoc_type": "App\\Ep",
  "type_id": "1",
  "body": "<p>The first responders race to rescue victims trapped in various tight spots. Meanwhile, Athena contemplates accepting a promotion that will take her out of the field, and Maddie decides to make a move of her own. Then, Buck wonders if he should move on, and Chimney finally deals with the aftermath of his near fatal car crash. Also, Eddie turns to the crew for help with his young son.</p>",
  "lang_id": "1",
  "created_at": "2018-09-28 04:21:19",
  "updated_at": "2018-09-28 04:21:19",
  "deleted_at": null
}
*/

internal data class DescriptionResponse(
        @SerializedName(DESCRIPTION_ID)
        var id: Long = 0,

        @SerializedName(DESCRIPTION_ASSOC_ID)
        var assocId: Long = 0,

        @SerializedName(DESCRIPTION_ASSOC_TYPE)
        var assocType: String = "",

        @SerializedName(DESCRIPTION_BODY)
        var body: String = "",

        @SerializedName(DESCRIPTION_TYPE_ID)
        var typeId: Long = 0,

        @SerializedName(DESCRIPTION_LANG_ID)
        var langId: Long = 0,

        @SerializedName(DESCRIPTION_UPDATED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var updatedAt: Long = 0,

        @SerializedName(DESCRIPTION_DELETED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var deletedAt: Long = 0
)

internal fun DescriptionResponse.toDO() =
        DescriptionDO(id, assocId, assocType, body, typeId, langId, updatedAt, deletedAt)

internal fun List<DescriptionResponse>.toDO() =
        map { it.toDO() }