package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.enums.SourceType
import iam.thevoid.oneom.data.db.model.SourceDO

data class Source(private val source: SourceDO) {

    val weight: Float
        get() = source.weight

    val name: String?
        get() = source.name

    val type: SourceType
        get() = source.type

    val active: Boolean
        get() = source.active

    val url: String?
        get() = source.url

    val search: String?
        get() = source.search

    val searchPage: String?
        get() = source.searchPage

    val searchStep: Int
        get() = source.searchStep

    val login: Boolean
        get() = source.login

    val data: String?
        get() = source.data

    internal val id: Long
        get() = source.id

}