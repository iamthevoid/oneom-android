package iam.thevoid.oneom.data.network.payload.response

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.network.payload.model.SerialResponse
import iam.thevoid.oneom.data.network.ONE_OM_SERIAL_DATA
import iam.thevoid.oneom.data.network.ONE_OM_SERIAL_SERIAL
import iam.thevoid.oneom.data.network.ONE_OM_SERIAL_URL
import iam.thevoid.oneom.data.network.ONE_OM_SERIAL_VIEW


internal data class SerialData(
        @SerializedName(ONE_OM_SERIAL_DATA) var payload: SerialPayload,
        @SerializedName(ONE_OM_SERIAL_VIEW) var view: String,
        @SerializedName(ONE_OM_SERIAL_URL) var url: String
)

internal data class SerialPayload(@SerializedName(ONE_OM_SERIAL_SERIAL) var serial: SerialResponse)
