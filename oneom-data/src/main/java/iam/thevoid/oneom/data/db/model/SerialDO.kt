package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class SerialDO (

        @PrimaryKey
        var id: Long = 0,
        var title: String = "",
        var langId: Long? = null,
        var weight: Float = 0f,
        var tvrageId: Long? = null,
        var tvmazeId: Long? = null,
        var mdbId: Long? = null,
        var tvdbId: Long? = null,
        var imdbId: String? = null,
        var imdbRating: Float? = null,
        var statusId: Long? = null,
        var posterId: Long? = null,
        var runtime: Float = 0f,
        var start: Long? = null,
        var end: Long? = null,
        var airtime: Long? = null,
        var airday: String? = null,
        var timezone: String? = null,
        var genres: String = "",
        var countries: String = "",
        var networks: String = "",
        var variants: String = "",
        var character: String = "",
        var people: String = "",
        var episodes: String = "",
        var description: String = "",
        val updatedAt : Long = 0,
        var isFullSummary : Boolean = false
)
