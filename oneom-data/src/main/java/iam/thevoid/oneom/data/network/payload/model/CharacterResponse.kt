package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.CharacterDO
import iam.thevoid.oneom.data.network.jsonadapter.TimestampLongAdapter

/*
{
  "id": 681,
  "tv_maze_id": "266183",
  "weight": "0.00",
  "name": "Morgan",
  "created_at": "2016-07-04 13:40:49",
  "updated_at": "2016-07-04 13:40:49",
  "deleted_at": null,
  "pivot": {
    "serial_id": "33095",
    "character_id": "681",
    "people_id": "596"
  },
  "file": []
},
*/

internal data class CharacterResponse(

        @SerializedName(CHARACTER_ID)
        var id: Long = 0L,

        @SerializedName(CHARACTER_NAME)
        var name: String = "",

        @SerializedName(CHARACTER_WEIGHT)
        var weight: Float = 0f,

        @SerializedName(CHARACTER_UPDATED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var updatedAt: Long = 0L,

        @SerializedName(CHARACTER_DELETED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var deletedAt: Long = 0L,

        @SerializedName(CHARACTER_TV_MAZE_ID)
        var tvMazeId: Long = 0,

        @SerializedName(CHARACTER_FILE)
        var files: List<FileResponse> = emptyList()
)

internal fun CharacterResponse.toDO() =
        CharacterDO(id, name, weight, updatedAt, deletedAt, tvMazeId, fileIds = files.joinToString(separator = ",") { "${it.id}" })

internal fun List<CharacterResponse>.toDO() =
        map { it.toDO() }