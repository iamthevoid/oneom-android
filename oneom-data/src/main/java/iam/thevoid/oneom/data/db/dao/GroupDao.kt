package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.GroupDO

@androidx.room.Dao
internal abstract class GroupDao : BaseDao<GroupDO>() {

    @Query("SELECT COUNT(*) FROM GroupDO")
    internal  abstract suspend fun count(): Long

}