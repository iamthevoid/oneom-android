package iam.thevoid.oneom.data.network

const val ONE_OM_CONFIG_IMAGE_BASE = "image_base"
const val ONE_OM_CONFIG_COUNTRY = "country"
const val ONE_OM_CONFIG_GENRE = "genre"
const val ONE_OM_CONFIG_LANG = "lang"
const val ONE_OM_CONFIG_SEX = "sex"
const val ONE_OM_CONFIG_NETWORK = "network"
const val ONE_OM_CONFIG_QUALITY = "quality"
const val ONE_OM_CONFIG_QUALITY_GROUP = "gquality"
const val ONE_OM_CONFIG_SOURCE = "source"
const val ONE_OM_CONFIG_STATUS = "status"
const val ONE_OM_CONFIG_GROUP = "group"

const val ONE_OM_EPISODE_EP = "ep"
const val ONE_OM_EPISODE_DATA = "data"
const val ONE_OM_EPISODE_VIEW = "view"
const val ONE_OM_EPISODE_URL = "url"

const val ONE_OM_EPISODES_DATA = "data"
const val ONE_OM_EPISODES_VIEW = "view"
const val ONE_OM_EPISODES_URL = "url"
const val ONE_OM_EPISODES_FUTURE_EPS = "future_eps"
const val ONE_OM_EPISODES_EPS = "eps"

const val ONE_OM_SERIAL_SERIAL = "serial"
const val ONE_OM_SERIAL_DATA = "data"
const val ONE_OM_SERIAL_VIEW = "view"
const val ONE_OM_SERIAL_URL = "url"
