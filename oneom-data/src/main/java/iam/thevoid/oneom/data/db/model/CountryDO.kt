package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CountryDO(
        @PrimaryKey
        var id: Long = 0,
        var weight: Float = 0f,
        var name: String? = null
)