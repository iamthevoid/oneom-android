package iam.thevoid.oneom.data

interface KVStorage {
    fun put(key: String, value: String)
    fun get(key: String, default: String): String
    fun get(key: String): String?
    fun delete(key: String)
}