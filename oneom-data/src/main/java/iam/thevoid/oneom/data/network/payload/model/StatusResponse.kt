package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.STATUS_ID
import iam.thevoid.oneom.data.STATUS_NAME
import iam.thevoid.oneom.data.db.model.StatusDO

/*
{
  "id": 1,
  "name": "Drama"
}
*/

internal data class StatusResponse(

        @SerializedName(STATUS_ID)
        var id: Long = 0,

        @SerializedName(STATUS_NAME)
        var name: String = ""

)

internal fun StatusResponse.toDO() =
        StatusDO(id, name)

internal fun List<StatusResponse>.toDO() =
        map { it.toDO() }