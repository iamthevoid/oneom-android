package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.CountryDO

@androidx.room.Dao
internal abstract class CountryDao : BaseDao<CountryDO>() {

    @Query("SELECT COUNT(*) FROM CountryDO")
    internal abstract suspend fun count(): Long
}