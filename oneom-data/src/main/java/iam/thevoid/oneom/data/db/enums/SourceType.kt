package iam.thevoid.oneom.data.db.enums

import com.google.gson.annotations.SerializedName

enum class SourceType {
    @SerializedName("1")
    TORRENT,
    @SerializedName("2")
    SUBTITLES,
    @SerializedName("3")
    ONLINE
}