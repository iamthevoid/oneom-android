package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.TorrentDO
import iam.thevoid.oneom.data.util.Size

data class Torrent(
    private val torrent: TorrentDO,
    val lang: Lang,
    val quality: Quality,
    val status: Status,
    val source: Source
) {

    val title: String
        get() = torrent.title

    val url: String
        get() = torrent.url

    val value: String?
        get() = torrent.value

    val size: Size by lazy { Size(torrent.size) }

    val seed: Int
        get() = torrent.seed

    val leech: Int
        get() = torrent.seed

    var vkPostId: Long = 0

    var rating: Long = 0

    internal val qualityId: Long
        get() = torrent.qualityId

    internal val statusId: Long
        get() = torrent.statusId

    internal val sourceId: Long
        get() = torrent.sourceId

    internal val langId: Long
        get() = torrent.langId

    internal val id: Long
        get() = torrent.id
}