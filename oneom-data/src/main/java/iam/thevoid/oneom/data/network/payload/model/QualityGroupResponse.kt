package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.QUALITY_GROUP_ID
import iam.thevoid.oneom.data.QUALITY_GROUP_NAME
import iam.thevoid.oneom.data.db.model.QualityGroupDO

/*
{
"id": 1,
"name": "Bad"
}
*/

internal data class QualityGroupResponse(
        @SerializedName(QUALITY_GROUP_ID)
        var id: Long = 0,

        @SerializedName(QUALITY_GROUP_NAME)
        var name: String = ""
)

internal fun QualityGroupResponse.toDO() =
        QualityGroupDO(id, name)

internal fun List<QualityGroupResponse>.toDO() =
        map { it.toDO() }