package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.LangDO

/*
{
  "id": 1,
  "name": "English",
  "short": "EN",
  "opensubtitles_short": "eng",
  "weight": 0
}
*/

internal data class LangResponse (
    @SerializedName(LANG_ID)
    var id : Long = 0,

    @SerializedName(LANG_WEIGHT)
    var weight : Float = 0f,

    @SerializedName(LANG_NAME)
    var name : String = "",

    @SerializedName(LANG_SHORT)
    var short : String = "",

    @SerializedName(LANG_OPEN_SUBTITLES_SHORT)
    var openSubtitlesShort : String = ""
)

internal fun LangResponse.toDO() =
        LangDO(id, weight, name, short, openSubtitlesShort)

internal fun List<LangResponse>.toDO() =
        map { it.toDO() }