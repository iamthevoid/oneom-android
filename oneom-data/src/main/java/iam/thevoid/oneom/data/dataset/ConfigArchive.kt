package iam.thevoid.oneom.data.dataset

import iam.thevoid.coroutines.retryWithDelay
import iam.thevoid.oneom.data.Storage
import iam.thevoid.oneom.data.dataset.ConfigArchive.Companion.IMAGE_BASE_STORAGE_KEY
import iam.thevoid.oneom.data.network.api.Web
import iam.thevoid.oneom.data.network.payload.model.toDO

interface ConfigArchive {

    suspend fun init()

    companion object {

        const val IMAGE_BASE_STORAGE_KEY: String = "IMAGE_BASE_STORAGE_KEY"
    }
}

internal class ConfigArchiveImplementation(private val web: Web) : ConfigArchive {

    override suspend fun init() {
        val config = retryWithDelay { web.config() }
        Storage.local.put(IMAGE_BASE_STORAGE_KEY, config.imageBase)
        Storage.database.countryDao.update(config.countries.toDO())
        Storage.database.genreDao.update(config.genres.toDO())
        Storage.database.langDao.update(config.lang.toDO())
        Storage.database.sexDao.update(config.sex.toDO())
        Storage.database.networkDao.update(config.networks.toDO())
        Storage.database.qualityGroupDao.update(config.qualityGroups.toDO())
        Storage.database.qualityDao.update(config.qualities.toDO())
        Storage.database.sourceDao.update(config.sources.toDO())
        Storage.database.statusDao.update(config.statuses.toDO())
        Storage.database.groupDao.update(config.groups.toDO())
    }
}