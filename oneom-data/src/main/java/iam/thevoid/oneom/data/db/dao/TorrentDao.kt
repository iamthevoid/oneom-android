package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.TorrentDO

@Dao
internal abstract class TorrentDao : BaseDao<TorrentDO>() {
    @Query("SELECT * FROM TorrentDO WHERE id in (:set)")
    internal abstract suspend fun byIdSet(set : List<String>) : List<TorrentDO>
}