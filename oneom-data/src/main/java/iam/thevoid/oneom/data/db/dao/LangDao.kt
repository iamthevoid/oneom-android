package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.LangDO

@androidx.room.Dao
internal abstract class LangDao : BaseDao<LangDO>() {

    @Query("SELECT COUNT(*) FROM LangDO")
    internal abstract suspend fun count(): Long

    @Query("SELECT * FROM LangDO WHERE id = :id")
    internal abstract suspend fun byId(id : Long): LangDO

}