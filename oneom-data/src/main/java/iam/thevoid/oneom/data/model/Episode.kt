package iam.thevoid.oneom.data.model

import iam.thevoid.noxml.recycler.DiffCallback
import iam.thevoid.oneom.data.db.model.EpisodeDO
import java.lang.StringBuilder
import java.text.DateFormat
import java.util.*

data class Episode(
    internal val episodeDO: EpisodeDO,
    val torrent: List<Torrent> = emptyList(),
    val subtitle: List<Subtitle> = emptyList(),
    val online: List<Online> = emptyList(),
    val description: List<Description> = emptyList()
) : DiffCallback.Diffable {

    val id
        get() = episodeDO.id

    val ep
        get() = episodeDO.ep

    val season: Int
        get() = episodeDO.season

    val weight: Float
        get() = episodeDO.weight

    val serialTitle: String
        get() = episodeDO.serialTitle

    val title: String
        get() = episodeDO.title

    val airDate: Date
        get() = episodeDO.airDate()

    val rate: String?
        get() = episodeDO.rait

    val updatedAt: Long
        get() = episodeDO.updatedAtLocal

    val onlineCount: Int
        get() = episodeDO.onlineCount

    val torrentCount: Int
        get() = episodeDO.torrentCount

    val subtitleCount: Int
        get() = episodeDO.subtitleCount

    fun isSameSerialAs(episode: Episode) =
        serialId == episode.serialId

    fun url() = "https://oneom.is/ep/$id"

    fun description() = description.firstOrNull()?.text

    fun se() = String.format("S%02dE%02d", season, ep)

    internal val serialId
        get() = episodeDO.serialId

    internal val tgPostId: Long?
        get() = episodeDO.tgPostId

    internal val fbPostId: Long?
        get() = episodeDO.fbPostId

    override fun areItemsTheSame(another: Any?): Boolean =
        another is Episode && another.id == id

    override fun areContentsTheSame(another: Any?): Boolean =
        another is Episode && another.episodeDO == episodeDO
}