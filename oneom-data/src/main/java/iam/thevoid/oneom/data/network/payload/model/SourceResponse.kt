package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.enums.SourceType
import iam.thevoid.oneom.data.db.model.SourceDO
import iam.thevoid.oneom.data.network.jsonadapter.BooleanAdapter
import iam.thevoid.oneom.data.network.jsonadapter.StringToIntAdapter

/*
{
  "id": 1,
  "name": "PirateBay",
  "type_id": 1,
  "active": 1,
  "weight": 0,
  "url": "https://thepiratebay.org/",
  "search": "https://thepiratebay.org/search/{searchString}/0/99/0",
  "search_page": "https://thepiratebay.org/search/{searchString}/{page}/99/0",
  "search_step": 1,
  "login": 0,
  "data": ""
}
*/

internal data class SourceResponse(

        @SerializedName(SOURCE_ID)
        var id: Long = 0,

        @SerializedName(SOURCE_WEIGHT)
        var weight: Float = 0f,

        @SerializedName(SOURCE_NAME)
        var name: String = "",

        @SerializedName(SOURCE_TYPE_ID)
        var type: SourceType,

        @SerializedName(SOURCE_ACTIVE)
        @JsonAdapter(BooleanAdapter::class)
        var active: Boolean = false,

        @SerializedName(SOURCE_URL)
        var url: String = "",

        @SerializedName(SOURCE_SEARCH)
        var search: String = "",

        @SerializedName(SOURCE_SEARCH_PAGE)
        var searchPage: String = "",

        @SerializedName(SOURCE_SEARCH_STEP)
        @JsonAdapter(StringToIntAdapter::class)
        var searchStep: Int = 0,

        @SerializedName(SOURCE_LOGIN)
        @JsonAdapter(BooleanAdapter::class)
        var login: Boolean = false,

        @SerializedName(SOURCE_DATA)
        var data: String = ""
)

internal fun SourceResponse.toDO() =
        SourceDO(id, weight, name, type, active, url, search, searchPage, searchStep, login, data)

internal fun List<SourceResponse>.toDO() =
        map { it.toDO() }