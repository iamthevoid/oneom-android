package iam.thevoid.oneom.data.dataset.episodes

import java.util.*

sealed class EpisodesFilter {

    data class Feed(val state: FeedState) : EpisodesFilter()

    data class ForDate(val date: Date) : EpisodesFilter()
}

enum class FeedState {

    /**
     * Possible leaks or viruses
     */
    FUTURE,

    /**
     * Released at the current date
     */
    RELEASED
}