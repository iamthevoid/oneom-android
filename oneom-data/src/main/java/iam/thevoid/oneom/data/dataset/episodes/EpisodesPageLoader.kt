@file:OptIn(ExperimentalTime::class)

package iam.thevoid.oneom.data.dataset.episodes

import iam.thevoid.coroutines.retryWithDelay
import iam.thevoid.e.safe
import iam.thevoid.noxml.coroutines.recycler.pagination.CoroutinesPageLoaderImpl
import iam.thevoid.noxml.recycler.paging.PageLoader
import iam.thevoid.oneom.data.Storage
import iam.thevoid.oneom.data.db.Database
import iam.thevoid.oneom.data.db.model.EpisodeDO
import iam.thevoid.oneom.data.network.api.Web
import iam.thevoid.oneom.data.util.Time
import java.util.*
import kotlin.time.ExperimentalTime
import kotlin.time.days

@OptIn(ExperimentalTime::class)
internal fun episodesPageLoader(web: Web)  =     CoroutinesPageLoaderImpl(
    startPage = 0,
    nextPage = { index -> index + 1 },
    loader = { page, refresh ->
        val dateBy = Date(System.currentTimeMillis() - page.days.inMilliseconds.toLong())
        val airdate = EpisodeDO.AIRDATE_DATE_FORMAT.format(dateBy)
        val episodes = with(Storage.database) {

            val dbCache = fromDb(airdate)

            // Refresh if no episodes on date or if
            if (refresh ||
                with(dbCache) {
                    isEmpty() ||
                            lastUpdateAtPageDate(dateBy) ||
                            smartCacheCondition(dateBy, page)
                }
            ) {
                val episodes = retryWithDelay { web.episodes(dateBy) }.payload.episodes
                updateEpisode(episodes)
                fromDb(airdate)
            } else {
                dbCache
            }
        }.map { Storage.database.getEpisode(it) }
        PageLoader.Page(episodes, page, false)
    }
)


private suspend fun Database.fromDb(airdate: String): List<EpisodeDO> {
    return episodeDao.byAirdate(airdate)
}

// The older airdate the less often update. 0-day and 1-day old episodes refreshes
// today (always) 2-day old refreshes every 1 day, 3-day - every 2 days, 4-day - every 3
// days etc
private fun List<EpisodeDO>.smartCacheCondition(dateBy: Date, page: Int): Boolean {
    return Time.dayDiff(Date(), (firstOrNull()?.airDate() ?: dateBy))
        .decrementWithZeroMin() == page.decrementWithZeroMin()
}

private fun Int.decrementWithZeroMin() = (this - 1).takeIf { it > 0 }.safe()

// last update was on airdate (maybe another episodes was released, but not loaded into db)
private fun List<EpisodeDO>.lastUpdateAtPageDate(dateBy: Date): Boolean {
    return Time.sameDate(dateBy, Date(firstOrNull()?.updatedAtLocal ?: dateBy.time))
}



