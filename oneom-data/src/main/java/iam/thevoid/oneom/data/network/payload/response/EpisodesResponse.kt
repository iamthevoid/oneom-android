package iam.thevoid.oneom.data.network.payload.response

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.network.*
import iam.thevoid.oneom.data.network.payload.model.EpisodeResponse


internal class EpisodesData(
        @SerializedName(ONE_OM_EPISODES_DATA) var payload: EpisodesPayload,
        @SerializedName(ONE_OM_EPISODES_VIEW) var view: String,
        @SerializedName(ONE_OM_EPISODES_URL) var url: String
)

internal class EpisodesPayload(
        @SerializedName(ONE_OM_EPISODES_FUTURE_EPS) var futureEps : List<EpisodeResponse> = emptyList(),
        @SerializedName(ONE_OM_EPISODES_EPS) var episodes : List<EpisodeResponse> = emptyList()
)