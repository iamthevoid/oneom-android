package iam.thevoid.oneom.data.network.payload.response

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.network.ONE_OM_EPISODE_DATA
import iam.thevoid.oneom.data.network.ONE_OM_EPISODE_EP
import iam.thevoid.oneom.data.network.ONE_OM_EPISODE_URL
import iam.thevoid.oneom.data.network.ONE_OM_EPISODE_VIEW
import iam.thevoid.oneom.data.network.payload.model.EpisodeResponse

internal data class EpisodeData(
        @SerializedName(ONE_OM_EPISODE_DATA) val payload: EpisodePayload,
        @SerializedName(ONE_OM_EPISODE_VIEW) val view: String,
        @SerializedName(ONE_OM_EPISODE_URL) val url: String
)

internal data class EpisodePayload(@SerializedName(ONE_OM_EPISODE_EP) val episode: EpisodeResponse)

