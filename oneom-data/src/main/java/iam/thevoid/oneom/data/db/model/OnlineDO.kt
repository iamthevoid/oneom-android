package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class OnlineDO (

    @PrimaryKey
    var id: Long = 0,
    var title: String = "",
    var url: String = "",
    var videoUrl: String = "",
    var trust: Boolean = false,
    var embedCode: String = "",
    var langId: Long = 0,
    var sourceId: Long = 0,
    var qualityId: Long = 0,
    var updatedAt: Long = 0,
    var deletedAt: Long = 0
)