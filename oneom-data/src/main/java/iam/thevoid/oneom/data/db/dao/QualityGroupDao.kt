package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.QualityGroupDO

@androidx.room.Dao
internal abstract class QualityGroupDao : BaseDao<QualityGroupDO>() {

    @Query("SELECT COUNT(*) FROM QualityGroupDO")
    internal abstract suspend fun count(): Long

}