package iam.thevoid.oneom.data.db.model

import android.annotation.SuppressLint
import androidx.room.Entity
import androidx.room.PrimaryKey
import iam.thevoid.oneom.data.db.ID_SEPARATOR
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class EpisodeDO(

    @PrimaryKey
    var id: Long = 0,

    var weight: Float = 0f,

    var serialId: Long = 0,

    var serialTitle: String = "",

    var season: Int = 0,

    var ep: Int = 0,

    var tgPostId: Long? = null,

    var fbPostId: Long? = null,

    var title: String = "",

    var airdate: String = "",

    var rait: String? = null,

    var onlineCount: Int = 0,

    var torrentCount: Int = 0,

    var subtitleCount: Int = 0,

    var updatedAtLocal: Long = 0,

    var updatedAt: Long = 0,

    var deletedAt: Long = 0,

    var torrent: String = "",

    var subtitles: String = "",

    var onlines: String = "",

    var descriptions: String = ""
) {
    companion object {

        val AIRDATE_DEFAULT = Date(0)

        @SuppressLint("SimpleDateFormat")
        val AIRDATE_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd")
    }

    @SuppressLint("SimpleDateFormat")
    fun airDate() = try {
        AIRDATE_DATE_FORMAT.parse(airdate) ?: AIRDATE_DEFAULT
    } catch (e: Exception) {
        AIRDATE_DEFAULT
    }

    val torrentIds
        get() = torrent.split(ID_SEPARATOR)
    val onlineIds
        get() = torrent.split(ID_SEPARATOR)
    val subtitleIds
        get() = torrent.split(ID_SEPARATOR)
}