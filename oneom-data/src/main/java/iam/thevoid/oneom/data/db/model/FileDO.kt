package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class FileDO(

        @PrimaryKey
        var id: Long = 0,
        var name: String = "",
        var path: String = "",
        var alt: String = "",
        var peopleId: Long? = null,
        var characterId: Long? = null
)