package iam.thevoid.oneom.data.db.dao

import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

internal fun SerialDao.observeExists(id : Long) = observeById(id)
        .filter { it.isNotEmpty() }
        .map { it.first() }

internal suspend fun SerialDao.await(id : Long) = observeExists(id).first()

internal suspend fun SerialDao.exists(id : Long) = count(id) != 0