package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LangDO (

    @PrimaryKey
    var id: Long = 0,

    var weight: Float = 0f,

    var name: String? = null,

    var shortName: String? = null,

    var openSubtitlesShort: String? = null
)