package iam.thevoid.oneom.data.dataset.episodes

import iam.thevoid.noxml.coroutines.recycler.pagination.CoroutinesPageLoader
import iam.thevoid.oneom.data.model.Episode
import iam.thevoid.oneom.data.model.Poster
import iam.thevoid.oneom.data.model.Serial
import kotlinx.coroutines.flow.Flow
import java.util.*

interface EpisodesArchive {

    fun episodesFilter(): Flow<EpisodesFilter>

    fun episode(id: Long): Flow<Episode>

    fun changeEpisodesFilter(filter: EpisodesFilter)

    fun episodesFeed(): CoroutinesPageLoader<Int, Episode>

    fun futureEpisodes(): Flow<List<Episode>>

    fun episodesForDate(date: Date): Flow<List<Episode>>

    fun serialFor(episode: Episode): Flow<Serial>

    fun posterFor(episode: Episode): Flow<Poster?>
}