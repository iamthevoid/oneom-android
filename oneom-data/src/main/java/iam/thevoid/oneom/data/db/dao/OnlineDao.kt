package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.OnlineDO
import iam.thevoid.oneom.data.db.model.TorrentDO

@Dao
internal abstract class OnlineDao : BaseDao<OnlineDO>() {
    @Query("SELECT * FROM OnlineDO WHERE id in (:set)")
    internal abstract suspend fun byIdSet(set: List<String>): List<OnlineDO>
}