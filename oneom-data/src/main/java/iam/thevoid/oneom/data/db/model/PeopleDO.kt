package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class PeopleDO(

        @PrimaryKey
        var id: Long = 0,
        var name: String = "",
        var weight: Float = 0f,
        var updatedAt: Long = 0,
        var deletedAt: Long = 0,
        var tvMazeId: Long = 0,
        var file: Long? = null
)