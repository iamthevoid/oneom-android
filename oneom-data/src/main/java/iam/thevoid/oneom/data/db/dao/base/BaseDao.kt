package iam.thevoid.oneom.data.db.dao.base

import androidx.room.*
import iam.thevoid.oneom.data.db.model.EpisodeDO

@Dao
internal abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun update(element: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun update(elements: List<T>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateSync(episodes: List<EpisodeDO>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateSync(episodes: EpisodeDO)

    @Delete
    abstract suspend fun delete(element: T)

    @Delete
    abstract suspend fun delete(elements: List<T>)

}