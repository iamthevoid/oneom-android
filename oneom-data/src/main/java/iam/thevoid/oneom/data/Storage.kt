package iam.thevoid.oneom.data

import android.content.Context
import androidx.room.Room
import iam.thevoid.oneom.data.db.DB_NAME
import iam.thevoid.oneom.data.db.Database


internal object Storage {

    internal lateinit var database: Database
    internal lateinit var local : KVStorage

    fun init (context: Context) {
        database = initDatabase(context)
        local = Local.init(context)
    }

    private fun initDatabase(context: Context): Database =
            Room.databaseBuilder(context.applicationContext, Database::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()

    @Suppress("unused", "UNUSED_PARAMETER")
    private fun initLocalStorage(context: Context) {

    }
}