package iam.thevoid.oneom.data.network.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import iam.thevoid.oneom.data.TimePattern
import iam.thevoid.oneom.data.json.Json
import iam.thevoid.oneom.data.network.payload.request.UserLoginRequest
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

internal class Web(json: Json) {

    companion object {
        const val HOST = "https://oneom.tk"
        const val poster_prefix = "https://fileom.s3.amazonaws.com/"
    }


    private val webService: WebService by lazy {
        mRetrofit.create(WebService::class.java)
    }

    private val userService: UserService by lazy {
        mRetrofit.create(UserService::class.java)
    }

    private val client: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.v(message)
                }
            }).apply { level = HttpLoggingInterceptor.Level.BODY })
            .build()
    }

    private val mRetrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(HOST)
            .addConverterFactory(GsonConverterFactory.create(json.gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client)
            .build()
    }

    suspend fun config() = webService.config()

    fun login(login: String, password: String) =
        userService.login(UserLoginRequest(login, password))

    fun reportError(error: Map<String, String>) =
        webService.reportError(error)

    suspend fun episode(id: Long) = webService.episodeById(id)

    suspend fun serial(id: Long) = webService.serialById(id)

    suspend fun episodes() = webService.episodes()

    suspend fun episodes(date: Date) =
        webService.episodesByDate(TimePattern.DATE.formatter.format(date))

}
