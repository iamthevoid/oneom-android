package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.ID_SEPARATOR
import iam.thevoid.oneom.data.db.model.EpisodeDO
import iam.thevoid.oneom.data.db.model.TorrentDO

/*
{
  "id": 3786,
  "title": "The Flash 1x11 [HDTv Ac3 Cas] By JBilbo",
  "value": "magnet:?xt=urn:btih:719160cf21ca88cecc64d30cbe2e86b7c1dda5f6&dn=The+Flash+1x11+%5BHDTv+Ac3+Cas%5D+By+JBilbo&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969",
  "url": "https://thepiratebay.sh/torrent/12219393/The_Flash_1x11_[HDTv_Ac3_Cas]_By_JBilbo",
  "size": 0,
  "seed": 0,
  "leech": 0,
  "lang_id": 1,
  "source_id": 1,
  "vk_post_id": "0",
  "quality_id": 5,
  "status_id": "1",
  "rating": null,
  "published_at": "2017-02-19 18:15:16",
  "created_at": "2015-08-11 22:30:13",
  "updated_at": "2015-08-11 22:30:13",
  "pivot": {
    "ep_id": "116710",
    "torrent_id": "3786",
    "created_at": "2015-12-07 10:03:28",
    "updated_at": "2015-12-07 10:03:28"
  },
  "source": {},
  "lang": {},
  "quality": {}
}
*/

internal data class TorrentResponse (
        @SerializedName(TORRENT_ID)
        var id : Long = 0,

        @SerializedName(TORRENT_TITLE)
        var title : String = "",

        @SerializedName(TORRENT_URL)
        var url : String = "",

        @SerializedName(TORRENT_VALUE)
        var value : String? = "",

        @SerializedName(TORRENT_SIZE)
        var size : Long = 0,

        @SerializedName(TORRENT_SEED)
        var seed : Int = 0,

        @SerializedName(TORRENT_LEECH)
        var leech : Int = 0,

        @SerializedName(TORRENT_LANG_ID)
        var langId : Long = 0,

        @SerializedName(TORRENT_VK_POST_ID)
        var vkPostId : Long = 0,

        @SerializedName(TORRENT_QUALITY_ID)
        var qualityId : Long = 0,

        @SerializedName(TORRENT_SOURCE_ID)
        var sourceId : Long = 0,

        @SerializedName(TORRENT_STATUS_ID)
        var statusId : Long = 0,

        @SerializedName(TORRENT_RATING)
        var rating : Long? = null
)

internal fun TorrentResponse.toDO() =
        TorrentDO(id, title, url, value, size, seed, leech, langId, vkPostId, qualityId, sourceId, statusId, rating.safe())

internal fun List<TorrentResponse>.toDO() =
        map { it.toDO() }