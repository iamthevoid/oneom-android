package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class GenreDO (

    @PrimaryKey
    var id: Long = 0,

    var name: String? = null
)