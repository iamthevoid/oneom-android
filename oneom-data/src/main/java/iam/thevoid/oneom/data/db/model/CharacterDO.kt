package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CharacterDO (

    @PrimaryKey
    var id: Long = 0L,
    var name: String = "",
    var weight: Float = 0f,
    var updatedAt: Long = 0L,
    var deletedAt: Long = 0L,
    var tvMazeId: Long = 0,
    var fileIds: String = ""
)