@file:OptIn(ExperimentalTime::class)
package iam.thevoid.oneom.data

import android.content.Context
import iam.thevoid.oneom.data.dataset.episodes.EpisodesArchive
import org.koin.core.module.Module
import timber.log.Timber
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.days

object DataLayer {

    object Config {
        internal var serialCacheDuration : Duration = 1.days
        internal var episodeCacheDuration : Duration = 30.days
    }

    fun init(context: Context, appModule: Module) {
        Storage.init(context)
        Timber.plant(Timber.DebugTree())
        initDi(appModule)
    }

}