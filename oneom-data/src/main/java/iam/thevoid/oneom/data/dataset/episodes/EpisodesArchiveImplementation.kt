@file:Suppress("EXPERIMENTAL_API_USAGE")

package iam.thevoid.oneom.data.dataset.episodes

import iam.thevoid.noxml.coroutines.data.CoroutineItem
import iam.thevoid.noxml.coroutines.recycler.pagination.CoroutinesPageLoader
import iam.thevoid.oneom.data.Storage
import iam.thevoid.oneom.data.dataset.SerialArchive
import iam.thevoid.oneom.data.db.model.EpisodeDO
import iam.thevoid.oneom.data.json.Json
import iam.thevoid.oneom.data.model.Episode
import iam.thevoid.oneom.data.model.Poster
import iam.thevoid.oneom.data.model.Serial
import iam.thevoid.oneom.data.network.api.Web
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import java.util.*

internal class EpisodesArchiveImplementation(
    private val json: Json,
    private val web: Web,
    private val serialArchive: SerialArchive
    ) :
    EpisodesArchive {

    companion object {
        private const val FILTER_KEY = "FILTER_KEY"
    }

    private val filter: CoroutineItem<EpisodesFilter> by lazy {
        CoroutineItem(
            Storage.local.get(FILTER_KEY)?.let { raw ->
                val resolver = json.fromJson<FilterResolver>(raw)
                if (resolver.isFeed()) {
                    json.fromJson<EpisodesFilter.Feed>(resolver.raw)
                } else {
                    json.fromJson<EpisodesFilter.ForDate>(resolver.raw)
                }
            } ?: EpisodesFilter.Feed(FeedState.RELEASED).also(::storeFilter))
    }

    override fun episodesFilter(): Flow<EpisodesFilter> = filter.observe()

    override fun episode(id: Long): Flow<Episode> =
        flowOf(Any())
            .map {
                with(Storage.database) {
                    getEpisode(episodeDao.episode(id))
                }
            }
            .flowOn(Dispatchers.IO)


    override fun changeEpisodesFilter(filter: EpisodesFilter) {
        this.filter.set(filter)
        storeFilter(filter)
    }

    override fun episodesFeed(): CoroutinesPageLoader<Int, Episode> = episodesPageLoader(web)

    override fun futureEpisodes(): Flow<List<Episode>> =
        with(Storage.database) {
            episodeDao.future()
                .flowOn(Dispatchers.IO)
                .map { list -> list.map { getEpisode(it) } }
                .onStart { updateEpisode(web.episodes().payload.futureEps) }
        }

    override fun episodesForDate(date: Date): Flow<List<Episode>> =
        with(Storage.database) {
            episodeDao.forDate(EpisodeDO.AIRDATE_DATE_FORMAT.format(date))
                .flowOn(Dispatchers.IO)
                .map { list -> list.map { getEpisode(it) } }
                .onStart { updateEpisode(web.episodes(date).payload.episodes) }
        }

    override fun serialFor(episode: Episode): Flow<Serial> =
        serialArchive.observeSerial(episode.serialId)

    override fun posterFor(episode: Episode): Flow<Poster?> =
        serialFor(episode).map { it.poster() }

    private fun storeFilter(filter: EpisodesFilter) {
        Storage.local.put(
            FILTER_KEY,
            json.toJson(FilterResolver(type = filter::class.java.simpleName, json.toJson(filter)))
        )
    }

    private data class FilterResolver(val type: String, val raw: String) {
        fun isFeed(): Boolean {
            return type == EpisodesFilter.Feed::class.java.simpleName
        }
    }
}
