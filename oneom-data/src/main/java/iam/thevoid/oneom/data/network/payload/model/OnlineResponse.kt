package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.OnlineDO
import iam.thevoid.oneom.data.db.model.SubtitleDO
import iam.thevoid.oneom.data.network.jsonadapter.BooleanAdapter
import iam.thevoid.oneom.data.network.jsonadapter.TimestampLongAdapter

/*
{
          "id": 10630,
          "title": "Pretty Little Liars S02E08 480p WEB DL nSD x264-NhaNc3",
          "url": "http://vodlocker.com/ju7fffnv3ahb",
          "video_url": "http://31.14.252.94:8777/6ocesx3d4k4pcnokajkspng2w4lzmomx66ydqm76gaeg4jak4xmoz75qxm/v.mp4",
          "trust": "0",
          "embed_code": "",
          "lang_id": "1",
          "source_id": "9",
          "quality_id": "7",
          "created_at": "-0001-11-30 00:00:00",
          "updated_at": "-0001-11-30 00:00:00",
          "deleted_at": null,
          "pivot": {
            "ep_id": "45131",
            "video_online_id": "10630",
            "created_at": "2016-02-22 18:24:04",
            "updated_at": "2016-02-22 18:24:04"
          },
          "source": {...},
          "lang": {...},
          "quality": {...}
        }
*/

internal data class OnlineResponse (

        @SerializedName(ONLINE_ID)
        var id : Long = 0,

        @SerializedName(ONLINE_TITLE)
        var title : String = "",

        @SerializedName(ONLINE_URL)
        var url : String = "",

        @SerializedName(ONLINE_VIDEO_URL)
        var videoUrl : String = "",

        @SerializedName(ONLINE_TRUST)
        @JsonAdapter(BooleanAdapter::class)
        var trust : Boolean = false,

        @SerializedName(ONLINE_EMBED_CODE)
        var embedCode : String = "",

        @SerializedName(ONLINE_LANG_ID)
        var langId : Long = 0,

        @SerializedName(ONLINE_SOURCE_ID)
        var sourceId : Long = 0,

        @SerializedName(ONLINE_QUALITY_ID)
        var qualityId : Long = 0,

        @SerializedName(ONLINE_UPDATED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var updatedAt: Long = 0,

        @SerializedName(ONLINE_DELETED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var deletedAt: Long = 0
)

internal fun OnlineResponse.toDO() =
        OnlineDO(id, title, url, videoUrl, trust, embedCode, langId, sourceId, qualityId, updatedAt, deletedAt)

internal fun List<OnlineResponse>.toDO() =
        map { it.toDO() }