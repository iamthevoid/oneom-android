package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.SexDO

@androidx.room.Dao
internal abstract class SexDao : BaseDao<SexDO>() {

    @Query("SELECT COUNT(*) FROM SexDO")
    internal abstract suspend fun count(): Long

}