package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import iam.thevoid.oneom.data.db.Converters
import iam.thevoid.oneom.data.db.enums.SourceType

@Entity
@TypeConverters(Converters::class)
class SourceDO (

    @PrimaryKey
    var id: Long = 0,

    var weight: Float = 0f,

    var name: String? = null,

    var type: SourceType,

    var active: Boolean = false,

    var url: String? = null,

    var search: String? = null,

    var searchPage: String? = null,

    var searchStep: Int = 0,

    var login: Boolean = false,

    var data: String? = null
)