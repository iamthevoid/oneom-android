package iam.thevoid.oneom.data.network.api


import iam.thevoid.oneom.data.network.payload.request.UserLoginRequest
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface UserService {

    @Headers("Accept:application/json")
    @POST("login")
    fun login(@Body request : UserLoginRequest) : Deferred<Any>

}
