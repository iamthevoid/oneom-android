package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.SEX_ID
import iam.thevoid.oneom.data.SEX_NAME
import iam.thevoid.oneom.data.db.model.SexDO

/*
{
  "id": 1,
  "name": "unknown",
  "created_at": null,
  "updated_at": null
}
*/

internal data class SexResponse(

        @SerializedName(SEX_ID)
        var id: Long = 0,

        @SerializedName(SEX_NAME)
        var name: String = ""
)

internal fun SexResponse.toDO() =
        SexDO(id, name)

internal fun List<SexResponse>.toDO() =
        map { it.toDO() }