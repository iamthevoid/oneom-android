package iam.thevoid.oneom.data.json

import com.google.gson.Gson
import kotlin.reflect.KClass

class Json(val gson: Gson) {

    fun <T> toJson(item: T): String = gson.toJson(item)

    fun <T : Any> fromJson(raw: String, cls: KClass<T>) = gson.fromJson(raw, cls.java)

    inline fun <reified T : Any> fromJson(raw: String) = fromJson(raw, T::class)
}