package iam.thevoid.oneom.data.network.payload.response

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.network.*
import iam.thevoid.oneom.data.network.payload.model.*

internal class ConfigResponse(
        @SerializedName(ONE_OM_CONFIG_IMAGE_BASE)
        var imageBase: String = "",

        @SerializedName(ONE_OM_CONFIG_COUNTRY)
        var countries: List<CountryResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_GENRE)
        var genres: List<GenreResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_LANG)
        var lang: List<LangResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_SEX)
        var sex: List<SexResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_NETWORK)
        var networks: List<NetworkResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_QUALITY_GROUP)
        var qualityGroups: List<QualityGroupResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_QUALITY)
        var qualities: List<QualityResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_SOURCE)
        var sources: List<SourceResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_STATUS)
        var statuses: List<StatusResponse> = emptyList(),

        @SerializedName(ONE_OM_CONFIG_GROUP)
        var groups: List<GroupResponse> = emptyList()
)