package iam.thevoid.oneom.data.network.jsonadapter

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

import java.io.IOException

/**
 * Created by iam on 03.04.17.
 */

class BooleanAdapter : TypeAdapter<Boolean>() {

    @Throws(IOException::class)
    override fun read(reader: JsonReader): Boolean {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull()
            return false
        }

        return reader.nextInt() != 0
    }

    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: Boolean?) {
        if (value == null) {
            out.nullValue()
            return
        }
        out.value((if (value) 1 else 0).toLong())
    }
}