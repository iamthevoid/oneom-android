package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.PosterDO
import iam.thevoid.oneom.data.db.model.SerialDO
import iam.thevoid.oneom.data.db.model.SubtitleDO
import iam.thevoid.oneom.data.db.model.TorrentDO

@Dao
internal abstract class PosterDao : BaseDao<PosterDO>() {
    @Query("SELECT * FROM PosterDO WHERE id in (:id)")
    internal abstract suspend fun byId(id : Long) : PosterDO
}