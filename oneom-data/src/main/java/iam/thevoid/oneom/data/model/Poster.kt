package iam.thevoid.oneom.data.model

import androidx.annotation.StringDef
import iam.thevoid.oneom.data.Storage
import iam.thevoid.oneom.data.dataset.ConfigArchive
import iam.thevoid.oneom.data.db.model.PosterDO

data class Poster(private val poster: PosterDO) {

    companion object {
        const val RESOLUTION_PATH_MAX = "max"
        const val RESOLUTION_PATH_480 = "480"
    }

    val name: String
        get() = poster.name
    val alt: String
        get() = poster.alt
    val path: String
        get() = poster.path

    @JvmOverloads
    fun url(@Resolution prefix: String = RESOLUTION_PATH_MAX) =
        "${Storage.local.get(ConfigArchive.IMAGE_BASE_STORAGE_KEY, "")}/$path/$prefix/$name"

    internal val id: Long
        get() = poster.id

    @StringDef(RESOLUTION_PATH_480, RESOLUTION_PATH_MAX)
    annotation class Resolution
}