package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.LangDO

data class Lang(private val lang: LangDO) {

    val weight: Float
        get() = lang.weight

    val name: String?
        get() = lang.name

    val short: String?
        get() = lang.shortName

    val openSubtitlesShort: String?
        get() = lang.openSubtitlesShort

    internal val id: Long
        get() = lang.id
}