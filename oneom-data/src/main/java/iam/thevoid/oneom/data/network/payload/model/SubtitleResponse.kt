package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.SubtitleDO
import iam.thevoid.oneom.data.db.model.TorrentDO
import iam.thevoid.oneom.data.network.jsonadapter.BooleanAdapter
import iam.thevoid.oneom.data.network.jsonadapter.TimeLongAdapter

/*
{
  "id": 10630,
  "title": "Pretty Little Liars S02E08 480p WEB DL nSD x264-NhaNc3",
  "url": "http://vodlocker.com/ju7fffnv3ahb",
  "video_url": "http://31.14.252.94:8777/6ocesx3d4k4pcnokajkspng2w4lzmomx66ydqm76gaeg4jak4xmoz75qxm/v.mp4",
  "trust": "0",
  "embed_code": "",
  "lang_id": "1",
  "source_id": "9",
  "quality_id": "7",
  "created_at": "-0001-11-30 00:00:00",
  "updated_at": "-0001-11-30 00:00:00",
  "deleted_at": null,
  "pivot": {
    "ep_id": "45131",
    "video_online_id": "10630",
    "created_at": "2016-02-22 18:24:04",
    "updated_at": "2016-02-22 18:24:04"
  },
  "source": {},
  "lang": {},
  "quality": {}
}
*/

internal data class SubtitleResponse (

        @SerializedName(SUBTITLE_ID)
        var id : Long = 0,

        @SerializedName(SUBTITLE_TITLE)
        var title : String = "",

        @SerializedName(SUBTITLE_URL)
        var url : String = "",

        @SerializedName(SUBTITLE_VIDEO_URL)
        var videoUrl : String = "",

        @SerializedName(SUBTITLE_TRUST)
        @JsonAdapter(BooleanAdapter::class)
        var trust : Boolean = false,

        @SerializedName(SUBTITLE_EMBED_CODE)
        @JsonAdapter(BooleanAdapter::class)
        var embedCode : String = "",

        @SerializedName(SUBTITLE_LANG_ID)
        var langId : Long = 0L,

        @SerializedName(SUBTITLE_SOURCE_ID)
        var sourceId : Long = 0L,

        @SerializedName(SUBTITLE_QUALITY_ID)
        var qualityId : Long = 0L,

        @SerializedName(SUBTITLE_UPDATED_AT)
        @JsonAdapter(TimeLongAdapter::class)
        var updatedAt : Long = 0L,

        @SerializedName(SUBTITLE_DELETED_AT)
        @JsonAdapter(TimeLongAdapter::class)
        var deletedAt : Long = 0L
)

internal fun SubtitleResponse.toDO() =
        SubtitleDO(id, title, url, videoUrl, trust, embedCode, langId, sourceId, qualityId, updatedAt, deletedAt)

internal fun List<SubtitleResponse>.toDO() =
        map { it.toDO() }