package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.TorrentDO
import iam.thevoid.oneom.data.db.model.VariantDO
import iam.thevoid.oneom.data.network.jsonadapter.DateLongAdapter
import iam.thevoid.oneom.data.network.jsonadapter.TimeLongAdapter

/*
{
  "id": 97,
  "serial_id": "100",
  "title": "The Killing Season",
  "start": "2015-06-09",
  "end": "0000-00-00",
  "created_at": "2017-01-28 13:35:07",
  "updated_at": "2017-01-28 13:35:07"
}
*/

internal data class VariantResponse(

        @SerializedName(VARIANT_ID)
        var id : Long = 0,

        @SerializedName(VARIANT_SERIAL_ID)
        var serialId : Long = 0,

        @SerializedName(VARIANT_TITLE)
        var title : String = "",

        @SerializedName(VARIANT_START)
        @JsonAdapter(DateLongAdapter::class)
        var start : Long = 0,

        @SerializedName(VARIANT_END)
        @JsonAdapter(DateLongAdapter::class)
        var end : Long = 0,

        @SerializedName(VARIANT_UPDATED_AT)
        @JsonAdapter(TimeLongAdapter::class)
        var updatedAt : Long = 0
)

internal fun VariantResponse.toDO() =
        VariantDO(id, serialId, title, start, end, updatedAt)

internal fun List<VariantResponse>.toDO() =
        map { it.toDO() }