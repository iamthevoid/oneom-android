package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.QUALITY_ID
import iam.thevoid.oneom.data.QUALITY_NAME
import iam.thevoid.oneom.data.QUALITY_QUALITY_GROUP_ID
import iam.thevoid.oneom.data.db.model.QualityDO

/*
{
  "id": 1,
  "quality_group_id": 1,
  "name": "CAM"
}
*/

internal data class QualityResponse(
        @SerializedName(QUALITY_ID)
        var id: Long = 0,

        @SerializedName(QUALITY_QUALITY_GROUP_ID)
        var weight: Long = 0,

        @SerializedName(QUALITY_NAME)
        var name: String = ""
)

internal fun QualityResponse.toDO() =
        QualityDO(id, weight, name)

internal fun List<QualityResponse>.toDO() =
        map { it.toDO() }