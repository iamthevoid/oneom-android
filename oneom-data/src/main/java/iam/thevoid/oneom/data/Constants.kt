@file:JvmName("DatabaseConstants")

package iam.thevoid.oneom.data

const val ALT = "alt"
const val DELETED_AT = "deleted_at"
const val FILE = "file"
const val ID = "id"
const val NAME = "name"
const val PATH = "path"
const val PIVOT = "pivot"
const val TITLE = "title"
const val TV_MAZE_ID = "tv_maze_id"
const val WEIGHT = "weight"
const val UPDATED_AT = "updated_at"
const val URL = "url"

const val CHARACTER_ID = ID
const val CHARACTER_NAME = NAME
const val CHARACTER_WEIGHT = WEIGHT
const val CHARACTER_UPDATED_AT = UPDATED_AT
const val CHARACTER_DELETED_AT = DELETED_AT
const val CHARACTER_TV_MAZE_ID = TV_MAZE_ID
const val CHARACTER_FILE = FILE
@Suppress("UNUSED") /* maybe need in future */ const val CHARACTER_PIVOT = PIVOT

// no sync
const val COUNTRY_ID = ID
const val COUNTRY_WEIGHT = WEIGHT
const val COUNTRY_NAME = NAME

const val DESCRIPTION_ID = ID
const val DESCRIPTION_UPDATED_AT = UPDATED_AT
const val DESCRIPTION_DELETED_AT = DELETED_AT
const val DESCRIPTION_ASSOC_ID = "assoc_id"
const val DESCRIPTION_ASSOC_TYPE = "assoc_type"
const val DESCRIPTION_BODY = "body"
const val DESCRIPTION_TYPE_ID = "type_id"
const val DESCRIPTION_LANG_ID = "lang_id"

const val EPISODE_ID = ID
const val EPISODE_WEIGHT = WEIGHT
const val EPISODE_TITLE = TITLE
const val EPISODE_UPDATED_AT = UPDATED_AT
const val EPISODE_DELETED_AT = DELETED_AT
const val EPISODE_SERIAL_ID = "serial_id"
const val EPISODE_SEASON = "season"
const val EPISODE_EP = "ep"
const val EPISODE_TG_POST_ID = "tg_post_id"
const val EPISODE_FB_POST_ID = "fb_post_id"
const val EPISODE_AIRDATE = "airdate"
const val EPISODE_RAIT = "rait"
const val EPISODE_ONLINE_COUNT = "online_c"
const val EPISODE_SUBTITLE_COUNT = "subtitle_c"
const val EPISODE_TORRENT_COUNT = "torrent_c"
const val EPISODE_SERIAL = "serial"
const val EPISODE_TORRENT = "torrent"
const val EPISODE_ONLINE = "online"
const val EPISODE_SUBTITLES = "subtitle"
const val EPISODE_DESCRIPTION = "description"
const val EPISODE_IS_FEED = "is_feed"

const val GROUP_ID = ID
const val GROUP_NAME = NAME
const val GROUP_TYPE_ID = "type_id"
const val GROUP_WEIGHT = WEIGHT
const val GROUP_FROM_LANG_ID = "from_lang_id"
const val GROUP_TO_LANG_ID = "to_lang_id"
const val GROUP_AVATAR_ID = "avatar_id"
const val GROUP_URL = URL

// no sync
const val FILE_ID = ID
const val FILE_NAME = NAME
const val FILE_ALT = ALT
const val FILE_PATH = PATH
const val FILE_PEOPLE_ID = "people_id"
const val FILE_CHARACTER_ID = "character_id"
@Suppress("UNUSED") /* maybe need in future */ const val FILE_PIVOT = PIVOT

// no sync
const val GENRE_ID = ID
const val GENRE_NAME = NAME

// no sync
const val LANG_ID = ID
const val LANG_NAME = NAME
const val LANG_WEIGHT = WEIGHT
const val LANG_OPEN_SUBTITLES_SHORT = "opensubtitles_short"
const val LANG_SHORT = "short"

// no sync
const val NETWORK_ID = ID
const val NETWORK_NAME = NAME
const val NETWORK_WEIGHT = WEIGHT
const val NETWORK_COUNTRY_ID = "country_id"

const val ONLINE_ID = ID
const val ONLINE_TITLE = TITLE
const val ONLINE_URL = URL
const val ONLINE_UPDATED_AT = UPDATED_AT
const val ONLINE_DELETED_AT = DELETED_AT
const val ONLINE_VIDEO_URL = "video_url"
const val ONLINE_TRUST = "trust"
const val ONLINE_EMBED_CODE = "embed_code"
const val ONLINE_LANG_ID = "lang_id"
const val ONLINE_SOURCE_ID = "source_id"
const val ONLINE_QUALITY_ID = "quality_id"
@Suppress("UNUSED") /* maybe need in future */ const val ONLINE_PIVOT = PIVOT
@Suppress("UNUSED") /* received in config */ const val ONLINE_SOURCE = "source"
@Suppress("UNUSED") /* received in config */ const val ONLINE_LANG = "lang"
@Suppress("UNUSED") /* received in config */ const val ONLINE_QUALITY = "quality"

const val PEOPLE_ID = ID
const val PEOPLE_NAME = NAME
const val PEOPLE_WEIGHT = WEIGHT
const val PEOPLE_UPDATED_AT = UPDATED_AT
const val PEOPLE_DELETED_AT = DELETED_AT
const val PEOPLE_TV_MAZE_ID = TV_MAZE_ID
const val PEOPLE_FILE = "file_id"
@Suppress("UNUSED") /* maybe need in future */ const val PEOPLE_PIVOT = PIVOT
@Suppress("UNUSED") /* received in config */ const val PEOPLE_SOURCE = "source"

const val POSTER_ID = ID
const val POSTER_NAME = NAME
const val POSTER_ALT = ALT
const val POSTER_PATH = PATH

// no sync
const val QUALITY_ID = ID
const val QUALITY_NAME = NAME
const val QUALITY_QUALITY_GROUP_ID = "quality_group_id"

// no sync
const val QUALITY_GROUP_ID = ID
const val QUALITY_GROUP_NAME = NAME

const val SERIAL_ID = ID
const val SERIAL_TITLE = TITLE
const val SERIAL_LANG_ID = "lang_id"
const val SERIAL_WEIGHT = WEIGHT
const val SERIAL_TVRAGE_ID = "tvrage_id"
const val SERIAL_TVMAZE_ID = "tvmaze_id"
const val SERIAL_MDB_ID = "mdb_id"
const val SERIAL_TVDB_ID = "tvdb_id"
const val SERIAL_IMDB_ID = "imdb_id"
const val SERIAL_IMDB_RATING = "imdb_rating"
const val SERIAL_STATUS_ID = "status_id"
const val SERIAL_POSTER_ID = "poster_id"
const val SERIAL_RUNTIME = "runtime"
const val SERIAL_START = "start"
const val SERIAL_END = "end"
const val SERIAL_AIRTIME = "airtime"
const val SERIAL_AIRDAY = "airday"
const val SERIAL_TIMEZONE = "timezone"
const val SERIAL_POSTER = "poster"
const val SERIAL_GENRE = "genre"
const val SERIAL_COUNTRY = "country"
const val SERIAL_NETWORK = "network"
const val SERIAL_VARIANT = "variant"
const val SERIAL_CHARACTER = "character"
const val SERIAL_PEOPLE = "people"
const val SERIAL_EP = "ep"
const val SERIAL_DESCRIPTION = "description"
@SuppressWarnings("UNUSED") /* received in config */ const val SERIAL_STATUS = "status"

// no sync
const val SOURCE_ID = ID
const val SOURCE_URL = URL
const val SOURCE_NAME = NAME
const val SOURCE_WEIGHT = WEIGHT
const val SOURCE_TYPE_ID = "type_id"
const val SOURCE_ACTIVE = "active"
const val SOURCE_SEARCH = "search"
const val SOURCE_SEARCH_PAGE = "search_page"
const val SOURCE_SEARCH_STEP = "search_step"
const val SOURCE_LOGIN = "login"
const val SOURCE_DATA = "data"

// no sync
const val SEX_ID = ID
const val SEX_NAME = NAME

// no sync
const val STATUS_ID = ID
const val STATUS_NAME = NAME

const val SUBTITLE_ID = ID
const val SUBTITLE_TITLE = TITLE
const val SUBTITLE_URL = URL
const val SUBTITLE_VIDEO_URL = "video_url"
const val SUBTITLE_TRUST = "trust"
const val SUBTITLE_EMBED_CODE = "embed_code"
const val SUBTITLE_LANG_ID = "lang_id"
const val SUBTITLE_SOURCE_ID = "source_id"
const val SUBTITLE_QUALITY_ID = "quality_id"
const val SUBTITLE_UPDATED_AT = "updated_at"
const val SUBTITLE_DELETED_AT = "deleted_at"
@Suppress("UNUSED") /* maybe need in future */ const val SUBTITLE_PIVOT = PIVOT
@Suppress("UNUSED") /* received in config */ const val SUBTITLE_SOURCE = "source"
@Suppress("UNUSED") /* received in config */ const val SUBTITLE_LANG = "lang"
@Suppress("UNUSED") /* received in config */ const val SUBTITLE_QUALITY = "quality"

const val TORRENT_ID = ID
const val TORRENT_TITLE = TITLE
const val TORRENT_URL = URL
const val TORRENT_VALUE = "value"
const val TORRENT_SIZE = "size"
const val TORRENT_SEED = "seed"
const val TORRENT_LEECH = "leech"
const val TORRENT_LANG_ID = "lang_id"
const val TORRENT_SOURCE_ID = "source_id"
const val TORRENT_VK_POST_ID = "vk_post_id"
const val TORRENT_QUALITY_ID = "quality_id"
const val TORRENT_STATUS_ID = "status_id"
const val TORRENT_RATING = "rating"
@Suppress("UNUSED") /* maybe need in future */ const val TORRENT_PIVOT = PIVOT
@Suppress("UNUSED") /* received in config */ const val TORRENT_SOURCE = "source"
@Suppress("UNUSED") /* received in config */ const val TORRENT_LANG = "lang"
@Suppress("UNUSED") /* received in config */ const val TORRENT_QUALITY = "quality"

const val VARIANT_ID = ID
const val VARIANT_SERIAL_ID = "serial_id"
const val VARIANT_TITLE = TITLE
const val VARIANT_START = "start"
const val VARIANT_END = "end"
const val VARIANT_UPDATED_AT = "updated_at"