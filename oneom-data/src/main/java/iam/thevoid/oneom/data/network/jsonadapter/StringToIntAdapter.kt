package iam.thevoid.oneom.data.network.jsonadapter

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

import java.io.IOException

/**
 * Created by iam on 03.04.17.
 */

class StringToIntAdapter : TypeAdapter<Int>() {

    @Throws(IOException::class)
    override fun read(reader: JsonReader): Int {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull()
            return 0
        }

        return try { reader.nextString().toInt() } catch (e : Exception) { 0 }
    }

    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: Int?) {
        if (value == null) {
            out.nullValue()
            return
        }

        out.value("$value")
    }
}