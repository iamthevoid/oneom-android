package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.QualityDO

data class Quality(private val quality: QualityDO) {

    val weight: Long
        get() = quality.weight

    val name: String?
        get() = quality.name

    internal val id: Long
        get() = quality.id
}