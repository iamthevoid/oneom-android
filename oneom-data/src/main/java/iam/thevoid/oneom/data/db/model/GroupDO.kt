package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/*
{
"id": 1,
"name": "NovaFilm",
"type_id": 1,
"weight": 0,
"from_lang_id": 1,
"to_lang_id": 2,
"avatar_id": 0,
"url": "http:\/\/novafilm.tv\/",
"created_at": null,
"updated_at": null,
"deleted_at": null
}
*/
@Entity
data class GroupDO(

        @PrimaryKey
        var id: Long = 0,

        var name: String? = null,

        var typeId: Long = 0,

        var weight: Double = .0,

        var fromLangId: Long = 0,

        var toLangId: Long = 0,

        var avatarId: Long = 0,

        var url: String? = null

)