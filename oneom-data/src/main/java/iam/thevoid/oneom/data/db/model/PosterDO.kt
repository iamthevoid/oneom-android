package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class PosterDO(

        @PrimaryKey
        var id: Long = 0,
        var name: String = "",
        var alt: String = "",
        var path: String = ""
)