package iam.thevoid.oneom.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import iam.thevoid.e.orElse
import iam.thevoid.oneom.data.db.model.FileDO
import iam.thevoid.oneom.data.db.dao.*
import iam.thevoid.oneom.data.db.model.*
import iam.thevoid.oneom.data.model.*
import iam.thevoid.oneom.data.network.payload.model.*
import iam.thevoid.oneom.data.network.payload.model.DescriptionResponse
import iam.thevoid.oneom.data.network.payload.model.EpisodeResponse
import iam.thevoid.oneom.data.network.payload.model.PosterResponse
import iam.thevoid.oneom.data.network.payload.model.SerialResponse
import iam.thevoid.oneom.data.network.payload.model.toDO


@Database(
    entities = [
        CharacterDO::class,
        CountryDO::class,
        DescriptionDO::class,
        EpisodeDO::class,
        FileDO::class,
        GenreDO::class,
        GroupDO::class,
        LangDO::class,
        NetworkDO::class,
        OnlineDO::class,
        PeopleDO::class,
        PosterDO::class,
        QualityDO::class,
        QualityGroupDO::class,
        SerialDO::class,
        SexDO::class,
        SourceDO::class,
        StatusDO::class,
        SubtitleDO::class,
        TorrentDO::class,
        VariantDO::class
    ], version = VERSION, exportSchema = false
)
internal abstract class Database : RoomDatabase() {
    abstract val characterDao: CharacterDao
    abstract val countryDao: CountryDao
    abstract val descriptionDao: DescriptionDao
    abstract val episodeDao: EpisodeDao
    abstract val fileDao: FileDao
    abstract val genreDao: GenreDao
    abstract val groupDao: GroupDao
    abstract val langDao: LangDao
    abstract val networkDao: NetworkDao
    abstract val onlineDao: OnlineDao
    abstract val qualityDao: QualityDao
    abstract val qualityGroupDao: QualityGroupDao
    abstract val peopleDao: PeopleDao
    abstract val posterDao: PosterDao
    abstract val serialDao: SerialDao
    abstract val sexDao: SexDao
    abstract val sharedDao: SharedDao
    abstract val sourceDao: SourceDao
    abstract val statusDao: StatusDao
    abstract val subtitleDao: SubtitleDao
    abstract val torrentDao: TorrentDao
    abstract val variantDao: VariantDao

    // section
    suspend fun updateCharacter(character: List<CharacterResponse>?) {
        character ?: return
        updateFile(character.map { it.files }.flatten())
        characterDao.update(character.toDO())
    }

    suspend fun updateCharacter(character: CharacterResponse?) {
        character ?: return
        updateFile(character.files)
        characterDao.update(character.toDO())
    }
    // end section


    // section
    suspend fun updateDescription(descrption: DescriptionResponse?) {
        descrption ?: return
        descriptionDao.update(descrption.toDO())
    }

    suspend fun updateDescription(descrption: List<DescriptionResponse>?) {
        descrption ?: return
        descriptionDao.update(descrption.toDO())
    }
    // end section


    // section
    suspend fun updateEpisode(episodes: List<EpisodeResponse>?) {
        episodes ?: return
        updateDescription(episodes.map { it.descriptions }.flatten())
        updateOnline(episodes.map { it.onlines }.flatten())
        updateSubtitle(episodes.map { it.subtitles }.flatten())
        updateTorrent(episodes.map { it.torrent }.flatten())
        updateSerial(episodes.mapNotNull { it.serial })
        episodeDao.update(episodes.toDO())
    }

    suspend fun getEpisode(episode: EpisodeDO): Episode {
        return Episode(
            episode,
            torrentDao.byIdSet(episode.torrentIds).map { getTorrent(it) },
            subtitleDao.byIdSet(episode.subtitleIds).map { getSubtitle(it) },
            onlineDao.byIdSet(episode.onlineIds).map { getOnline(it) }
        )
    }
    // end section


    // section
    suspend fun updateFile(file: List<FileResponse>?) {
        file ?: return
        fileDao.update(file.toDO())
    }

    suspend fun updateFile(file: FileResponse?) {
        file ?: return
        fileDao.update(file.toDO())
    }
    // end section


    // section
    suspend fun getLang(lang: LangDO): Lang {
        return Lang(lang)
    }
    // end section


    // section
    suspend fun updateOnline(online: List<OnlineResponse>?) {
        online ?: return
        onlineDao.update(online.toDO())
    }

    suspend fun getOnline(online: OnlineDO): Online {
        return Online(
            online,
            getLang(langDao.byId(online.langId)),
            getQuality(qualityDao.byId(online.qualityId)),
            getSource(sourceDao.byId(online.sourceId))
        )
    }
    // end section


    // section
    suspend fun getQuality(quality: QualityDO) = Quality(quality)
    // end section


    // section
    suspend fun updatePeople(people: List<PeopleResponse>?) {
        people ?: return
        updateFile(people.mapNotNull { it.file })
        peopleDao.update(people.toDO())
    }

    suspend fun updatePeople(people: PeopleResponse?) {
        people ?: return
        updateFile(people.file)
        peopleDao.update(people.toDO())
    }
    // end section


    // section
    suspend fun updatePoster(poster: List<PosterResponse>?) {
        poster ?: return
        posterDao.update(poster.toDO())
    }

    suspend fun updatePoster(poster: PosterResponse?) {
        poster ?: return
        posterDao.update(poster.toDO())
    }
    // end section


    // section
    suspend fun updateSerial(serial: List<SerialResponse>?, isFullSummary: Boolean = false) {
        serial ?: return
        updatePoster(serial.mapNotNull { it.poster })
//        updateEpisode(serial.episodes)
        updateDescription(serial.mapNotNull { it.description }.flatten())
        updateVariant(serial.mapNotNull { it.variants }.flatten())
        updateCharacter(serial.mapNotNull { it.character }.flatten())
        updatePeople(serial.mapNotNull { it.people }.flatten())
        serialDao.update(serial.toDO().onEach { it.isFullSummary = isFullSummary })
    }

    suspend fun updateSerial(serial: SerialResponse?, isFullSummary: Boolean = false) {
        serial ?: return
        updatePoster(serial.poster)
//        updateEpisode(serial.episodes)
        updateDescription(serial.description)
        updateVariant(serial.variants)
        updateCharacter(serial.character)
        updatePeople(serial.people)
        serialDao.update(serial.toDO().also { it.isFullSummary = isFullSummary })
    }
    // end section


    // section
    suspend fun getStatus(status: StatusDO) = Status(status)
    // end section


    // section
    suspend fun getSource(source: SourceDO) = Source(source)
    // end section


    // section
    suspend fun updateSubtitle(subtitle: List<SubtitleResponse>?) {
        subtitle ?: return
        subtitleDao.update(subtitle.toDO())
    }

    suspend fun getSubtitle(subtitle: SubtitleDO): Subtitle {
        return Subtitle(
            subtitle,
            getLang(langDao.byId(subtitle.langId)),
            getSource(sourceDao.byId(subtitle.sourceId)),
            getQuality(qualityDao.byId(subtitle.qualityId))
        )
    }
    // end section


    // section
    suspend fun updateTorrent(torrent: List<TorrentResponse>?) {
        torrent ?: return
        torrentDao.update(torrent.toDO())
    }

    suspend fun getTorrent(torrent: TorrentDO): Torrent {
        return Torrent(
            torrent,
            getLang(langDao.byId(torrent.langId)),
            getQuality(qualityDao.byId(torrent.qualityId)),
            getStatus(statusDao.byId(torrent.statusId.orElse(StatusDO.UNKNOWN_STATUS_ID))),
            getSource(sourceDao.byId(torrent.sourceId))
        )
    }
    // end section


    //section
    suspend fun updateVariant(variant: List<VariantResponse>?) {
        variant ?: return
        variantDao.update(variant.toDO())
    }

    suspend fun updateVariant(variant: VariantResponse?) {
        variant ?: return
        variantDao.update(variant.toDO())
    }
    // ens section
}
