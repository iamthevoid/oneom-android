package iam.thevoid.oneom.data.dataset

import iam.thevoid.oneom.data.Storage
import iam.thevoid.oneom.data.model.Poster

interface PosterArchive {

    suspend fun poster(id : Long): Poster

    companion object {
        val instance: PosterArchive by lazy(::PosterArchiveImplementation)
    }
}

private class PosterArchiveImplementation : PosterArchive {

    override suspend fun poster(id: Long): Poster {
        return Poster(Storage.database.posterDao.byId(id))
    }
}