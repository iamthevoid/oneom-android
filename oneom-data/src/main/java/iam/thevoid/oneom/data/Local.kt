package iam.thevoid.oneom.data

import android.content.Context
import android.content.SharedPreferences

internal object Local : KVStorage {

    lateinit var preferences: SharedPreferences

    fun init(context: Context) = apply {
        preferences = context.getSharedPreferences(javaClass.simpleName, Context.MODE_PRIVATE)
    }

    override fun put(key: String, value: String) =
            preferences.edit().putString(key, value).apply()

    override fun get(key: String, default: String): String =
            get(key) ?: default

    override fun get(key: String): String? =
            preferences.getString(key, null)

    override fun delete(key: String) =
            preferences.edit().remove(key).apply()
}