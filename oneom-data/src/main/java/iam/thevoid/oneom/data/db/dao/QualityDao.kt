package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.LangDO
import iam.thevoid.oneom.data.db.model.QualityDO

@androidx.room.Dao
internal abstract class QualityDao : BaseDao<QualityDO>() {

    @Query("SELECT COUNT(*) FROM QualityDO")
    internal abstract suspend fun count(): Long

    @Query("SELECT * FROM QualityDO WHERE id = :id")
    internal abstract suspend fun byId(id : Long): QualityDO

}