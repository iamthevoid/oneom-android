package iam.thevoid.oneom.data.util

import android.annotation.SuppressLint
import java.lang.Math.abs
import java.text.SimpleDateFormat
import java.util.*

object Time {

    const val MILLISECOND = 1L
    const val SECOND = 1000 * MILLISECOND
    const val MINUTE = 60 * SECOND
    const val HOUR = 60 * MINUTE
    const val DAY = 24 * HOUR

    const val UNIXEPOCH_SECOND = SECOND / 1000
    const val UNIXEPOCH_MINUTE = MINUTE / 1000
    const val UNIXEPOCH_HOUR = HOUR / 1000
    const val UNIXEPOCH_DAY = DAY / 1000

    private val DATE_FORMAT = "dd-MM-yyyy"

    fun daysFromEpoch(timeMillis: Long) = timeMillis / DAY

    fun sameDate(date1: Date, date2: Date) =
        daysFromEpoch(date1.clearTime().time) == daysFromEpoch(date2.clearTime().time)

    fun dayDiff(date1: Date, date2: Date) =
        abs(daysFromEpoch(date1.clearTime().time) - daysFromEpoch(date2.clearTime().time)).toInt()


    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @SuppressLint("SimpleDateFormat")
    private fun Date.clearTime(): Date {
        return SimpleDateFormat(DATE_FORMAT).run { parse(format(this@clearTime)) }
    }
}