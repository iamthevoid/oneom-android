package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.COUNTRY_ID
import iam.thevoid.oneom.data.COUNTRY_NAME
import iam.thevoid.oneom.data.COUNTRY_WEIGHT
import iam.thevoid.oneom.data.db.model.CountryDO

/*
{
"id": 1,
"weight": 0,
"name": "US"
}
*/

internal data class CountryResponse(

        @SerializedName(COUNTRY_ID)
        var id: Long = 0,

        @SerializedName(COUNTRY_WEIGHT)
        var weight: Float = 0f,

        @SerializedName(COUNTRY_NAME)
        var name: String = ""

)

internal fun CountryResponse.toDO() =
        CountryDO(id, weight, name)

internal fun List<CountryResponse>.toDO() =
        map { it.toDO() }

