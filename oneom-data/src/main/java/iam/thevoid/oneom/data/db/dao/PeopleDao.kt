package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.PeopleDO
import iam.thevoid.oneom.data.db.model.SubtitleDO
import iam.thevoid.oneom.data.db.model.TorrentDO

@Dao
internal abstract class PeopleDao : BaseDao<PeopleDO>() {
    @Query("SELECT * FROM PeopleDO WHERE id in (:set)")
    internal abstract suspend fun byIdSet(set : String)  : List<PeopleDO>
}