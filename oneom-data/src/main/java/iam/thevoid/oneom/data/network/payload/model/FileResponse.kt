package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.CharacterDO
import iam.thevoid.oneom.data.db.model.FileDO

/*
{
  "id": 1040560,
  "name": "99668.jpg.jpg",
  "alt": "Judy Joo",
  "path": "people",
  "pivot": {
    "people_id": "21548",
    "file_id": "1040560"
  }
}
*/

internal data class FileResponse(

        @SerializedName(FILE_ID)
        var id: Long = 0,

        @SerializedName(FILE_NAME)
        var name: String = "",

        @SerializedName(FILE_PATH)
        var path: String = "",

        @SerializedName(FILE_ALT)
        var alt: String = "",

        @SerializedName(FILE_PEOPLE_ID)
        var peopleId: Long? = null,

        @SerializedName(FILE_CHARACTER_ID)
        var characterId: Long? = null
)

internal fun FileResponse.toDO() =
        FileDO(id, name, path, alt, peopleId, characterId)

internal fun List<FileResponse>.toDO() =
        map { it.toDO() }