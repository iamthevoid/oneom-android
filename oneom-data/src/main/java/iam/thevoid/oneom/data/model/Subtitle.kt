package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.SubtitleDO

data class Subtitle(
    private val subtitle: SubtitleDO,
    val lang: Lang,
    val source: Source,
    val quality: Quality
) {
    val title: String
        get() = subtitle.title

    val url: String
        get() = subtitle.url

    val videoUrl: String
        get() = subtitle.videoUrl

    val trust: Boolean
        get() = subtitle.trust

    val embedCode: String
        get() = subtitle.embedCode


    val id: Long
        get() = subtitle.id

    internal val langId: Long
        get() = subtitle.langId

    internal val sourceId: Long
        get() = subtitle.sourceId

    internal val qualityId: Long
        get() = subtitle.qualityId

    internal val updatedAt: Long
        get() = subtitle.updatedAt

    internal val deletedAt: Long
        get() = subtitle.deletedAt
}