package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TorrentDO(

    @PrimaryKey
    var id: Long = 0,
    var title: String = "",
    var url: String = "",
    var value: String? = "",
    var size: Long = 0,
    var seed: Int = 0,
    var leech: Int = 0,
    var langId: Long = 0,
    var vkPostId: Long = 0,
    var qualityId: Long = 0,
    var sourceId: Long = 0,
    var statusId: Long = 0,
    var rating: Long = 0
)