package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.*

@Dao
internal abstract class FileDao : BaseDao<FileDO>() {
    @Query("SELECT * FROM FileDO WHERE id in (:set)")
    internal abstract suspend fun byIdSet(set : String)  : List<FileDO>
}