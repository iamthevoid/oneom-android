package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class StatusDO(

    @PrimaryKey
    var id: Long = 0,

    var name: String? = null
) {
    companion object {
        const val UNKNOWN_STATUS_ID = 13L
    }
}