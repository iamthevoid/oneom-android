package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class NetworkDO(

        @PrimaryKey
        var id: Long = 0,

        var name: String? = null,

        var weight: Float = 0f,

        var countryId: Long = 0
)