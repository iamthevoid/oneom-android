package iam.thevoid.oneom.data.network.jsonadapter

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.TimePattern
import java.io.IOException
import java.util.*

// 2016-07-04 13:40:49

class TimestampLongAdapter : TypeAdapter<Long>() {

    @Throws(IOException::class)
    override fun read(`in`: JsonReader): Long {
        if (`in`.peek() == JsonToken.NULL) {
            `in`.nextNull()
            return 0L
        }
        return parseToLong(`in`.nextString())
    }

    @Synchronized
    private fun parseToLong(json: String): Long = try {
        val parse: Date? = TimePattern.TIMESTAMP.formatter.parse(json)
        parse?.time.safe()
    } catch (e: Exception) {
        0L
    }

    @Synchronized
    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: Long?) {
        if (value == null) {
            out.nullValue()
            return
        }
        out.value(TimePattern.TIMESTAMP.formatter.format(value))
    }
}