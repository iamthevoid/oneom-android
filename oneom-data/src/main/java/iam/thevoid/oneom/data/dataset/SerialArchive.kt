package iam.thevoid.oneom.data.dataset

import iam.thevoid.coroutines.retryWithDelay
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.Storage
import iam.thevoid.oneom.data.db.dao.await
import iam.thevoid.oneom.data.db.dao.exists
import iam.thevoid.oneom.data.db.dao.observeExists
import iam.thevoid.oneom.data.model.Serial
import iam.thevoid.oneom.data.network.api.Web
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.util.concurrent.ConcurrentHashMap
import kotlin.time.ExperimentalTime
import kotlin.time.days

interface SerialArchive {

    suspend fun serial(id: Long): Serial

    fun observeSerial(id: Long): Flow<Serial>

}

internal class SerialArchiveImplementation(private val web: Web) : SerialArchive,
    CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val updating = ConcurrentHashMap<Long, Boolean>()

    override suspend fun serial(id: Long): Serial {
        updateSerialIfNeeded(id)
        return Serial(Storage.database.serialDao.await(id))
    }

    override fun observeSerial(id: Long): Flow<Serial> {
        launch { updateSerialIfNeeded(id) }
        return Storage.database.serialDao.observeExists(id).map { Serial(it) }
    }

    @OptIn(ExperimentalTime::class)
    private suspend fun updateSerialIfNeeded(id: Long) = with(Storage.database.serialDao) {
        val exists = exists(id)
        val needsToRefresh = exists && byId(id).let { serial ->
            val isTooOld = serial.updatedAt <= (System.currentTimeMillis() - 1.days.inMilliseconds)
            !serial.isFullSummary || isTooOld
        }

        if (!updating[id].safe() && (!exists || needsToRefresh)) {
            updating[id] = true
            val serial = retryWithDelay { web.serial(id).payload.serial }
            Storage.database.updateSerial(serial, isFullSummary = true)
            updating.remove(id)
        }
    }
}