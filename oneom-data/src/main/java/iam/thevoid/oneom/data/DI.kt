package iam.thevoid.oneom.data

import com.google.gson.Gson
import iam.thevoid.oneom.data.dataset.ConfigArchive
import iam.thevoid.oneom.data.dataset.ConfigArchiveImplementation
import iam.thevoid.oneom.data.dataset.SerialArchive
import iam.thevoid.oneom.data.dataset.SerialArchiveImplementation
import iam.thevoid.oneom.data.dataset.episodes.EpisodesArchive
import iam.thevoid.oneom.data.dataset.episodes.EpisodesArchiveImplementation
import iam.thevoid.oneom.data.json.Json
import iam.thevoid.oneom.data.network.api.Web
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.dsl.single

fun initDi(appModule: Module) {
    startKoin {
        modules(oneOmDataLayerCoreModule)
        modules(archiveModule)
        modules(appModule)
    }
}

private val oneOmDataLayerCoreModule = module {
    single<Gson>()
    single<Json>()
    single<Web>()
}

private val archiveModule = module {
    single<EpisodesArchive> { EpisodesArchiveImplementation(get(), get(), get()) }
    single<SerialArchive> { SerialArchiveImplementation(get()) }
    single<ConfigArchive> { ConfigArchiveImplementation(get()) }
}
