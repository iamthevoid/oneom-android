package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.GenreDO
import kotlinx.coroutines.flow.Flow

@androidx.room.Dao
internal abstract class GenreDao : BaseDao<GenreDO>() {

    @Query("SELECT COUNT(*) FROM GenreDO")
    internal abstract suspend fun count(): Long

    @Query("SELECT * FROM GenreDO")
    internal abstract fun all() : Flow<List<GenreDO>>

    @Query("DELETE FROM GenreDO")
    internal abstract suspend fun deleteAll()
}