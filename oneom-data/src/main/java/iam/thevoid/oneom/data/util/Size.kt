package iam.thevoid.oneom.data.util

import iam.thevoid.e.format
import iam.thevoid.e.safe

class Size(val bytes: Long) {

    val units: Units
    val mains: Int
    val endings: Int

    init {
        var current = bytes;
        var mains = (current / 1024).toInt()
        var ending = (current % 1024).toInt()
        var level = 0
        while (current / 1024 > 0) {
            mains = (current / 1024).toInt()
            ending = (current % 1024).toInt()
            current /= 1024
            level++
        }
        this.units = Units.values()[level]
        this.mains = mains
        this.endings = ending
    }


    private val mainsGrade
        get() = grade(mains)

    private val endingsGrade
        get() = grade(endings)

    override fun toString(): String {
        val spaceLeft = 3 - mainsGrade

        val full = "$mains.$endings"
            .toFloat()
            .format(spaceLeft.takeIf { it > 0 }.safe(1))
            .let { "$it$units" }

        return if (mains == 0) "$endings$units" else full
    }


    private fun grade(ending: Int): Int {
        return when {
            ending / 10 == 0 -> 1
            ending / 100 == 0 -> 2
            ending / 1024 == 0 -> 3
            else -> 0
        }
    }


    enum class Units {
        B, KB, MB, GB, TB, PB, EB, ZB, YB, TO_MUCH
    }
}