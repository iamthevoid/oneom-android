package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.ID_SEPARATOR
import iam.thevoid.oneom.data.db.model.EpisodeDO
import iam.thevoid.oneom.data.network.jsonadapter.TimestampLongAdapter

/*
{
      "id": 15004,
      "serial_id": "51",
      "season": "10",
      "ep": "58",
      "tg_post_id": null,
      "fb_post_id": null,
      "title": "Thursday 1st October 1981",
      "airdate": "1981-10-01",
      "rait": null,
      "weight": "0.00",
      "online_c": "0",
      "subtitle_c": "0",
      "torrent_c": "0",
      "updated_at": "2017-04-11 17:00:05",
      "serial": {
        "id": 51,
        "title": "Emmerdale",
        "lang_id": "1",
        "tvrage_id": "3440",
        "tvmaze_id": "2548",
        "mdb_id": "2527",
        "tvdb_id": "3440",
        "imdb_id": "http://www.imdb.com/title/tt0068069",
        "imdb_rating": "0.00",
        "weight": "1622.00",
        "status_id": "13",
        "poster_id": "50",
        "runtime": "30",
        "start": "1972-10-16",
        "end": "2018-09-28",
        "airtime": "19:00",
        "airday": "Daily",
        "timezone": "GMT+0 +DST",
        "updated_at": "2018-09-24 01:19:05",
        "poster": {
          "id": 50,
          "name": "51.jpg",
          "alt": "Emmerdale",
          "path": "serial"
        }
      },
      "torrent": [],
      "online": [],
      "subtitle": [],
      "description": []
    }
*/
internal data class EpisodeResponse(

        @SerializedName(EPISODE_ID)
         var id: Long = 0,

        @SerializedName(EPISODE_WEIGHT)
         var weight: Float = 0f,

        @SerializedName(EPISODE_SERIAL_ID)
         var serialId: Long = 0,

        @SerializedName(EPISODE_SEASON)
         var season: Int = 0,

        @SerializedName(EPISODE_EP)
         var ep: Int = 0,

        @SerializedName(EPISODE_TG_POST_ID)
         var tgPostId: Long? = null,

        @SerializedName(EPISODE_FB_POST_ID)
         var fbPostId: Long? = null,

        @SerializedName(EPISODE_TITLE)
         var title: String = "",

        @SerializedName(EPISODE_AIRDATE)
         var airdate: String? = null,

        @SerializedName(EPISODE_RAIT)
         var rait: String? = null,

        @SerializedName(EPISODE_ONLINE_COUNT)
         var onlineCount: Int = 0,

        @SerializedName(EPISODE_TORRENT_COUNT)
         var torrentCount: Int = 0,

        @SerializedName(EPISODE_SUBTITLE_COUNT)
         var subtitleCount: Int = 0,

        @SerializedName(EPISODE_UPDATED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
         var updatedAt: Long = 0,

        @SerializedName(EPISODE_DELETED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
         var deletedAt: Long = 0,

        @SerializedName(EPISODE_SERIAL)
        var serial: SerialResponse? = null,

        @SerializedName(EPISODE_TORRENT)
        var torrent: List<TorrentResponse> = emptyList(),

        @SerializedName(EPISODE_SUBTITLES)
        var subtitles: List<SubtitleResponse> = emptyList(),

        @SerializedName(EPISODE_ONLINE)
        var onlines: List<OnlineResponse> = emptyList(),

        @SerializedName(EPISODE_DESCRIPTION)
        var descriptions: List<DescriptionResponse> = emptyList()
)

internal fun EpisodeResponse.toDO() =
        EpisodeDO(
                id = id,
                weight = weight,
                serialTitle = serial?.title.safe(),
                serialId = serialId,
                season = season,
                ep = ep,
                tgPostId = tgPostId,
                fbPostId = fbPostId,
                title = title,
                airdate = airdate.safe(),
                rait = rait,
                onlineCount = onlineCount,
                torrentCount = torrentCount,
                subtitleCount = subtitleCount,
                updatedAtLocal = System.currentTimeMillis(),
                updatedAt = updatedAt,
                deletedAt = deletedAt,
                torrent = torrent.safe().joinToString(ID_SEPARATOR) { "${it.id}" },
                subtitles = subtitles.safe().joinToString(ID_SEPARATOR) { "${it.id}" },
                onlines = onlines.safe().joinToString(ID_SEPARATOR) { "${it.id}" },
                descriptions = descriptions.safe().joinToString(ID_SEPARATOR) { "${it.id}" }

        )

internal fun List<EpisodeResponse>.toDO() =
        map { it.toDO() }