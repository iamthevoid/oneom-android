package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.TorrentDO
import iam.thevoid.oneom.data.db.model.VariantDO

@Dao
internal abstract class VariantDao : BaseDao<VariantDO>() {
    @Query("SELECT * FROM VariantDO WHERE id in (:set)")
    internal abstract suspend fun byIdSet(set : String) : List<VariantDO>
}