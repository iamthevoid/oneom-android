@file:OptIn(ExperimentalTime::class)

package iam.thevoid.oneom.data.dataset

import iam.thevoid.oneom.data.DataLayer
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

interface GlobalConfig {


    val serialCacheTime : Duration
    val episodeCacheTime : Duration

    companion object {
        val instance : GlobalConfig by lazy { GlobalConfigImplementation() }
    }
}

private class GlobalConfigImplementation : GlobalConfig {

    override val serialCacheTime: Duration
        get() = DataLayer.Config.serialCacheDuration

    override val episodeCacheTime: Duration
        get() = DataLayer.Config.episodeCacheDuration

}