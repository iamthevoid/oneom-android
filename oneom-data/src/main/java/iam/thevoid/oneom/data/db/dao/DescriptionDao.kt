package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.DescriptionDO
import iam.thevoid.oneom.data.db.model.TorrentDO

@Dao
internal abstract class DescriptionDao : BaseDao<DescriptionDO>() {
    @Query("SELECT * FROM DescriptionDO WHERE id in (:set)")
    internal abstract suspend fun byIdSet(set : String)  : List<DescriptionDO>
}