package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.StatusDO

data class Status(private val status: StatusDO) {

    val name: String?
        get() = status.name

    internal val id: Long
        get() = status.id

}