package iam.thevoid.oneom.data.model

import iam.thevoid.oneom.data.db.model.SerialDO
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.dataset.PosterArchive
import java.util.*

data class Serial(private val serialDo: SerialDO) {

    val title: String
        get() = serialDo.title
    val runtime: Float
        get() = serialDo.runtime
    val weight: Float
        get() = serialDo.weight
    val imdbRating: Float?
        get() = serialDo.imdbRating
    val start: Date
        get() = Date(serialDo.start.safe())
    val end: Date
        get() = Date(serialDo.end.safe())
    val airtime: Date?
        get() = serialDo.airtime?.let(::Date)
    val airday: String?
        get() = serialDo.airday
    val timezone: String?
        get() = serialDo.timezone

    suspend fun poster() = posterId?.let { PosterArchive.instance.poster(it) }

    internal val id: Long
        get() = serialDo.id
    internal val langId: Long?
        get() = serialDo.langId
    internal val tvrageId: Long?
        get() = serialDo.tvrageId
    internal val tvmazeId: Long?
        get() = serialDo.tvmazeId
    internal val mdbId: Long?
        get() = serialDo.mdbId
    internal val tvdbId: Long?
        get() = serialDo.tvdbId
    internal val imdbId: String?
        get() = serialDo.imdbId
    internal val statusId: Long?
        get() = serialDo.statusId
    internal val posterId: Long?
        get() = serialDo.posterId
    internal val genres: String
        get() = serialDo.genres
    internal val countries: String
        get() = serialDo.countries
    internal val networks: String
        get() = serialDo.networks
    internal val variants: String
        get() = serialDo.variants
    internal val character: String
        get() = serialDo.character
    internal val people: String
        get() = serialDo.people
    internal val episodes: String
        get() = serialDo.episodes
    internal val description: String
        get() = serialDo.description
}
