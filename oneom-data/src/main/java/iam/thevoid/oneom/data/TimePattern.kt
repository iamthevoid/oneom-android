package iam.thevoid.oneom.data

import java.text.SimpleDateFormat
import java.util.*

enum class TimePattern(private val pattern: String) {

    // 2016-07-04 13:40:49
    TIMESTAMP("yyyy-MM-dd hh:mm:ss"),
    // 2016-07-04
    DATE("yyyy-MM-dd"),
    // 13:00
    TIME("hh:mm");

    val formatter by lazy { SimpleDateFormat(pattern, Locale.getDefault()) }
}