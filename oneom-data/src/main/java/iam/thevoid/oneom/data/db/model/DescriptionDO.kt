package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class DescriptionDO (

    @PrimaryKey
    var id: Long = 0,
    var assocId: Long = 0,
    var assocType: String = "",
    var body: String = "",
    var typeId: Long = 0,
    var langId: Long = 0,
    var updatedAt: Long = 0,
    var deletedAt: Long = 0
)