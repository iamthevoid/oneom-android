package iam.thevoid.oneom.data.network.api


import iam.thevoid.oneom.data.network.payload.response.ConfigResponse
import iam.thevoid.oneom.data.network.payload.response.EpisodeData
import iam.thevoid.oneom.data.network.payload.response.EpisodesData
import iam.thevoid.oneom.data.network.payload.response.SerialData
import kotlinx.coroutines.Deferred
import retrofit2.http.*

internal interface WebService {

    @Headers("Accept: application/json")
    @GET("/data/config")
    suspend fun config() : ConfigResponse

    @Headers("Accept: application/json")
    @GET("/ep/{id}")
    suspend fun episodeById(@Path("id") id: Long) : EpisodeData

    @Headers("Accept: application/json")
    @GET("/serial/{id}")
    suspend fun serialById(@Path("id") id: Long) : SerialData

    @Headers("Accept: application/json")
    @GET("/ep/date/{date}")
    suspend fun episodesByDate(@Path("date") date: String) : EpisodesData

    @Headers("Accept: application/json")
    @GET("/ep")
    suspend fun episodes() : EpisodesData

    @FormUrlEncoded
    @POST("/errors/java")
    fun reportError(@FieldMap data: Map<String, String>): Deferred<Any>

    // LEGACY

//    @get:GET("/data/config")
//    val initialData: Call<DataConfig>

//    @get:GET("/ep")
//    val lastEpisodes: Observable<Eps>

//    @get:GET("/ep")
//    val lastEpisodesFlat: Call<Eps>

//    @GET("search/ep")
//    fun getEpisodesByDateObservable(@Query("start") start: String, @Query("end") end: String): Observable<EpsByDate>

//    @GET("search/ep")
//    fun getEpisodesByDate(@Query("start") start: String, @Query("end") end: String): Call<EpsByDate>

//    @GET("/serial/{id}")
//    fun getSerial(@Path("id") id: Long): Observable<SerialPayload>

//    @GET("/search/serial")
//    fun searchSerials(@Query("title") searchString: String): Observable<SerialsSearch>

//    @GET("/ep/{id}")
//    fun getEpisode(@Path("id") id: Long): Observable<Ep>

    @FormUrlEncoded
    @POST("/errors/java")
    suspend fun sendError(@FieldMap data: Map<String, String>)
}
