package iam.thevoid.oneom.data.db.dao

import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.NetworkDO

@androidx.room.Dao
internal abstract class NetworkDao : BaseDao<NetworkDO>() {

    @Query("SELECT COUNT(*) FROM NetworkDO")
    internal abstract suspend fun count(): Long

}