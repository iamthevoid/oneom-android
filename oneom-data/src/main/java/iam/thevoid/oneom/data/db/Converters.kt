package iam.thevoid.oneom.data.db

import androidx.room.TypeConverter
import iam.thevoid.oneom.data.db.enums.SourceType

class Converters {
    @TypeConverter
    fun fromSourceType(sourceType: SourceType): String {
        return sourceType.name
    }

    @TypeConverter
    fun toSourceType(name: String): SourceType {
        return SourceType.valueOf(name)
    }
}