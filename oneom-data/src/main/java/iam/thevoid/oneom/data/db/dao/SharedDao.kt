package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query

@Dao
internal abstract class SharedDao {


    /**
     * PUBLIC
     */
    internal suspend fun initialDataAlreadyReceived(): Boolean = countConfigData() > 0


    /**
     * QUERIES
     */
    @Query("""
        SELECT (
            (SELECT COUNT(*) FROM CountryDO) +
            (SELECT COUNT(*) FROM GenreDO) +
            (SELECT COUNT(*) FROM GroupDO) +
            (SELECT COUNT(*) FROM LangDO) +
            (SELECT COUNT(*) FROM NetworkDO) +
            (SELECT COUNT(*) FROM QualityDO) +
            (SELECT COUNT(*) FROM QualityGroupDO) +
            (SELECT COUNT(*) FROM SexDO) +
            (SELECT COUNT(*) FROM SourceDO) +
            (SELECT COUNT(*) FROM StatusDO)
        )
    """)
    internal abstract suspend fun countConfigData(): Long
}