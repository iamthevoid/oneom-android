package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.GENRE_ID
import iam.thevoid.oneom.data.GENRE_NAME
import iam.thevoid.oneom.data.db.model.GenreDO

/*
{
  "id": 1,
  "name": "Drama"
}
*/

internal data class GenreResponse(

        @SerializedName(GENRE_ID)
        var id: Long = 0,

        @SerializedName(GENRE_NAME)
        var name: String = ""
)

internal fun GenreResponse.toDO() =
        GenreDO(id, name)

internal fun List<GenreResponse>.toDO() =
        map { it.toDO() }