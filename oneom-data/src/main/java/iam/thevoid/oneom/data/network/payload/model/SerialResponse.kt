package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.ID_SEPARATOR
import iam.thevoid.oneom.data.db.model.EpisodeDO
import iam.thevoid.oneom.data.db.model.SerialDO
import iam.thevoid.oneom.data.network.jsonadapter.DateLongAdapter
import iam.thevoid.oneom.data.network.jsonadapter.TimeLongAdapter

/*
{
      "id": 100,
      "title": "The Killing Season",
      "lang_id": null,
      "tvrage_id": "49520",
      "tvmaze_id": null,
      "mdb_id": null,
      "tvdb_id": null,
      "imdb_id": "http://www.imdb.com/title/tt0536437",
      "imdb_rating": "0.00",
      "weight": "141.00",
      "status_id": "7",
      "poster_id": "95",
      "runtime": "70",
      "start": "2015-06-09",
      "end": "0000-00-00",
      "airtime": "20:00",
      "airday": "Tuesday",
      "timezone": "GMT+10 -DST",
      "updated_at": "2017-04-29 05:05:06",
      "poster": {...},
      "status": {...},
      "genre": [{..."pivot": {"serial_id": "100","genre_id": "37"}}],
      "country": [{..."pivot": {"serial_id": "100","country_id": "6"}}],
      "network": [{..."pivot": {"serial_id": "100","network_id": "59"}}],
      "variant": [{}],
      "character": [{}],
      "people": [{}],
      "ep": [{}],
      "description": [{}]
    }
  }
*/

internal data class SerialResponse(

        @SerializedName(SERIAL_ID)
        var id: Long = 0,

        @SerializedName(SERIAL_TITLE)
        var title: String = "",

        @SerializedName(SERIAL_LANG_ID)
        var langId: Long? = null,

        @SerializedName(SERIAL_WEIGHT)
        var weight: Float = 0f,

        @SerializedName(SERIAL_TVRAGE_ID)
        var tvrageId: Long? = null,

        @SerializedName(SERIAL_TVMAZE_ID)
        var tvmazeId: Long? = null,

        @SerializedName(SERIAL_MDB_ID)
        var mdbId: Long? = null,

        @SerializedName(SERIAL_TVDB_ID)
        var tvdbId: Long? = null,

        @SerializedName(SERIAL_IMDB_ID)
        var imdbId: String? = null,

        @SerializedName(SERIAL_IMDB_RATING)
        var imdbRating: Float? = null,

        @SerializedName(SERIAL_STATUS_ID)
        var statusId: Long? = null,

        @SerializedName(SERIAL_POSTER_ID)
        var posterId: Long? = null,

        @SerializedName(SERIAL_RUNTIME)
        var runtime: Float = 0f,

        @SerializedName(SERIAL_START)
        @JsonAdapter(DateLongAdapter::class)
        var start: Long? = null,

        @SerializedName(SERIAL_END)
        @JsonAdapter(DateLongAdapter::class)
        var end: Long? = null,

        @SerializedName(SERIAL_AIRTIME)
        @JsonAdapter(TimeLongAdapter::class)
        var airtime: Long? = null,

        @SerializedName(SERIAL_AIRDAY)
        var airday: String? = null,

        @SerializedName(SERIAL_TIMEZONE)
        var timezone: String? = null,

        @SerializedName(SERIAL_POSTER)
        var poster: PosterResponse? = null,

        @SerializedName(SERIAL_EP)
        var episodes: List<EpisodeResponse>? = emptyList(),

        @SerializedName(SERIAL_DESCRIPTION)
        var description: List<DescriptionResponse>? = emptyList(),

        @SerializedName(SERIAL_VARIANT)
        var variants: List<VariantResponse>? = emptyList(),

        @SerializedName(SERIAL_CHARACTER)
        var character: List<CharacterResponse>? = emptyList(),

        @SerializedName(SERIAL_PEOPLE)
        var people: List<PeopleResponse>? = emptyList(),

        @SerializedName(SERIAL_GENRE)
        var genres: List<GenreResponse>? = emptyList(),

        @SerializedName(SERIAL_COUNTRY)
        var countries: List<CountryResponse>? = emptyList(),

        @SerializedName(SERIAL_NETWORK)
        var networks: List<NetworkResponse>? = emptyList()
)

internal fun SerialResponse.toDO() =
        SerialDO(
                id,
                title,
                langId,
                weight,
                tvrageId,
                tvmazeId,
                mdbId,
                tvdbId,
                imdbId,
                imdbRating,
                statusId,
                posterId,
                runtime,
                start,
                end,
                airtime,
                airday,
                timezone,
                episodes = episodes.safe().joinToString(separator = ",") { "${it.id}" },
                description = description.safe().joinToString(separator = ",") { "${it.id}" },
                variants = variants.safe().joinToString(separator = ",") { "${it.id}" },
                character = character.safe().joinToString(separator = ",") { "${it.id}" },
                people = people.safe().joinToString(separator = ",") { "${it.id}" },
                genres = genres.safe().joinToString(separator = ",") { "${it.id}" },
                countries = countries.safe().joinToString(separator = ",") { "${it.id}" },
                networks = networks.safe().joinToString(separator = ",") { "${it.id}" },
                updatedAt = System.currentTimeMillis()
        )

internal fun List<SerialResponse>.toDO() =
        map { it.toDO() }
