package iam.thevoid.oneom.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import iam.thevoid.oneom.data.db.dao.base.BaseDao
import iam.thevoid.oneom.data.db.model.SerialDO
import iam.thevoid.oneom.data.db.model.SubtitleDO
import iam.thevoid.oneom.data.db.model.TorrentDO
import kotlinx.coroutines.flow.Flow

@Dao
internal abstract class SerialDao : BaseDao<SerialDO>() {
    @Query("SELECT * FROM SerialDO WHERE id in (:id)")
    internal abstract suspend fun byId(id : Long) : SerialDO

    @Query("SELECT COUNT(*) FROM SerialDO WHERE id in (:id)")
    internal abstract suspend fun count(id : Long) : Int

    @Query("SELECT * FROM SerialDO WHERE id in (:id)")
    internal abstract fun observeById(id : Long) : Flow<List<SerialDO>>
}