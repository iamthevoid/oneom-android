package iam.thevoid.oneom.data.network.payload.model

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import iam.thevoid.oneom.data.*
import iam.thevoid.oneom.data.db.model.FileDO
import iam.thevoid.oneom.data.db.model.PeopleDO
import iam.thevoid.oneom.data.network.jsonadapter.TimestampLongAdapter

/*
 {
  "id": 21548,
  "name": "Judy Joo",
  "tv_maze_id": "113065",
  "weight": "0.00",
  "created_at": "2016-07-06 22:52:21",
  "updated_at": "2016-07-06 22:52:21",
  "deleted_at": null,
  "pivot": {
    "serial_id": "38255",
    "people_id": "21548",
    "character_id": "50622"
  },
  "file": [{}],
  "source": [{}]
}
*/

internal data class PeopleResponse(

        @SerializedName(PEOPLE_ID)
        var id : Long = 0,

        @SerializedName(PEOPLE_NAME)
        var name : String = "",

        @SerializedName(PEOPLE_WEIGHT)
        var weight : Float = 0f,

        @SerializedName(PEOPLE_UPDATED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var updatedAt : Long = 0,

        @SerializedName(PEOPLE_DELETED_AT)
        @JsonAdapter(TimestampLongAdapter::class)
        var deletedAt : Long = 0,

        @SerializedName(PEOPLE_TV_MAZE_ID)
        var tvMazeId : Long = 0,

        @SerializedName(PEOPLE_FILE)
        var file : FileResponse? = null
)

internal fun PeopleResponse.toDO() =
        PeopleDO(id, name, weight, updatedAt, deletedAt, tvMazeId, file = file?.id)

internal fun List<PeopleResponse>.toDO() =
        map { it.toDO() }