package iam.thevoid.oneom.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.network.payload.model.TorrentResponse

@Entity
class SubtitleDO (

    @PrimaryKey
    var id: Long = 0,
    var title: String = "",
    var url: String = "",
    var videoUrl: String = "",
    var trust: Boolean = false,
    var embedCode: String = "",
    var langId: Long = 0L,
    var sourceId: Long = 0L,
    var qualityId: Long = 0L,
    var updatedAt: Long = 0L,
    var deletedAt: Long = 0L
)