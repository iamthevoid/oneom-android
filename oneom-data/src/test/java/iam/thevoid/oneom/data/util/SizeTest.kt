package iam.thevoid.oneom.data.util

import org.junit.Assert
import org.junit.Test

class SizeTest {
    @Test
    fun sizeTest() {
        checkSizeMatches("664MB", 696223006L)
        checkSizeMatches("1.3MB", 1024L * 1024L + 1024L * 300L)
        checkSizeMatches("1GB", 1024L * 1024L * 1024L)
        checkSizeMatches("1MB", 1024L * 1024L)
        checkSizeMatches("1.57MB", 1024L * 1024L + 1024L * 567L)
        checkSizeMatches("19.5TB", 1024L * 1024L * 1024L * 20000L)
        checkSizeMatches("195.3TB", 1024L * 1024L * 1024L * 200000L)
        checkSizeMatches("138.3MB", 144736202L)
        checkSizeMatches("4.15GB", 4450011649L)
        checkSizeMatches("864B", 864L)
        checkSizeMatches("0B", 0L)
    }

    fun checkSizeMatches(expected: String, bytes: Long) {
        val size = Size(bytes)
        println(size)
        Assert.assertEquals(expected, "$size")
    }
}