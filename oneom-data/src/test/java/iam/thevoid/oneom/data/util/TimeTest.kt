package iam.thevoid.oneom.data.util

import iam.thevoid.e.hour
import org.junit.Assert
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class TimeTest {

    @Test
    fun sameDayTest() {
        testSameDayParsed()
        testSameDayCalculated()
    }

    private fun testSameDayParsed() {
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val date1 = simpleDateFormat.parse("02-03-1995")!!
        val date2 = simpleDateFormat.parse("02-03-1995")!!
        val date3 = simpleDateFormat.parse("01-03-1995")!!
        val date4 = simpleDateFormat.parse("03-03-1995")!!
        val date5 = simpleDateFormat.parse("08-11-2000")!!

        Assert.assertTrue(Time.sameDate(date1, date2))
        Assert.assertFalse(Time.sameDate(date1, date3))
        Assert.assertFalse(Time.sameDate(date1, date4))
        Assert.assertFalse(Time.sameDate(date1, date5))
    }

    private fun testSameDayCalculated() {
        val date1 = Date(System.currentTimeMillis())
        val date2 = Date(System.currentTimeMillis())
        val date3 = Date(System.currentTimeMillis() - (Date().hour() + 1) * 1000 * 60 * 600)
        val date4 = Date(System.currentTimeMillis() + (24 - Date().hour() + 1) * 1000 * 60 * 600)
        Assert.assertTrue(Time.sameDate(date1, date2))
        Assert.assertFalse(Time.sameDate(date1, date3))
        Assert.assertFalse(Time.sameDate(date1, date4))
    }
}