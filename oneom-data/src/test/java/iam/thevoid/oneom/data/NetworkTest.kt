package iam.thevoid.oneom.data

import com.google.gson.Gson
import iam.thevoid.e.safe
import iam.thevoid.oneom.data.network.api.Web
import iam.thevoid.oneom.data.network.payload.model.EpisodeResponse
import iam.thevoid.oneom.data.network.payload.model.SerialResponse
import iam.thevoid.oneom.data.network.payload.response.EpisodeData
import iam.thevoid.oneom.data.network.payload.response.EpisodePayload
import iam.thevoid.oneom.data.network.payload.response.EpisodesData
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import java.util.*

const val EPISODE_COUNT = 1_739_269
const val SERIAL_COUNT = 51_293

class NetworkTest {

    @Test
    fun login() {
//        runBlocking {
//            val login = Web.login("iam", "5555").await()
//            print(login)
//        }
    }

    @Test
    fun getInitialData() {
        runBlocking {
            val config = Web.config()
            config
        }
        println()
    }

    @Test
    fun getRandomSerial() {
//        val id = Random().nextInt(1000).toLong()
//        try {
//            runBlocking { Web.serial(id) }
//        } catch (e: Exception) {
//            getRandomSerial()
//        }
//        Assert.assertTrue(true)
    }

    @Test
    fun getSerialById() {
//        val serial = runBlocking { Web.serial(58817) }
//
//        Assert.assertTrue(true)
    }

    @Test
    fun findDesc() = getEpisode(1, { it + 100 }) { it.descriptions.isNotEmpty() }

    @Test
    fun findOnlines() = getEpisodes { list -> list.any { it.onlines.isNotEmpty() } }

    @Test
    fun findTorrent() = getEpisodes { list -> list.any { it.torrent.isNotEmpty() } }

    @Test
    fun findSubtitles() = getEpisodes { list -> list.any { it.subtitles.isNotEmpty() } }

    @Test
    fun findSerial() = getEpisode {  it.serial != null  }

    @Test
    fun randomEpisode() = getEpisodes()

    private fun getEpisodes(
            breakCondition: (List<EpisodeResponse>) -> Boolean = { true }
    ) {
        Assert.assertTrue(true)

        var current: EpisodesData? = null
        val calendar = Calendar.getInstance()
        while (calendar.get(Calendar.YEAR) > 2017 || (current != null && current.payload.episodes.isNotEmpty())) {
            try {
                current = runBlocking { Web.episodes(calendar.time) }
                if (breakCondition(current.payload.episodes)) {
                    println(current.payload.episodes.find { it.subtitles.isNotEmpty() })
                    break
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            calendar.add(Calendar.DAY_OF_MONTH, -1)
        }
    }

    @Test
    fun findVariant() = getSerial { it.variants.safe().isNotEmpty() }

    @Test
    fun findSerialDesc() = getSerial { it.description.safe().isNotEmpty() }

    @Test
    fun findCharacters() = getSerial { it.character.safe().isNotEmpty() }

    @Test
    fun findCharactersWithFile() = getSerial { it.character.safe().isNotEmpty() && it.character.safe().first().files.isNotEmpty() }

    @Test
    fun findPeople() = getSerial { it.people.safe().isNotEmpty() }

    private fun getEpisode(
            initial: Long = Random().nextInt(EPISODE_COUNT).toLong() + 1,
            next: (Long) -> Long = { Random().nextInt(EPISODE_COUNT).toLong() + 1 },
            breakCondition: (EpisodeResponse) -> Boolean = { true }
    ) {
        Assert.assertTrue(true)
        val usedIds = mutableSetOf<Long>()
        val exceptionTypes = hashMapOf<Class<*>, Int>()

        var id = initial
        runBlocking {
            while (true) {
                try {
                    val data = Web.episode(id)
                    if (breakCondition(data.payload.episode)) {
                        println("Found ${data.payload.episode}")
                        break
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    exceptionTypes[e.javaClass] = (exceptionTypes[e.javaClass] ?: 0) + 1
                    if (e is NoMoreException)
                        break
                }

                if (usedIds.size == SERIAL_COUNT)
                    break

                usedIds += id
                while (usedIds.contains(id))
                    id = next(id)
            }
        }

        println("Finished for ${usedIds.size} requests")
        println("Next exceptions was thrown $exceptionTypes")
        println("Id's was requested = $usedIds")
    }

    private fun getSerial(
            initial: Long = Random().nextInt(SERIAL_COUNT).toLong() + 1,
            next: (Long) -> Long = { Random().nextInt(SERIAL_COUNT).toLong() + 1 },
            breakCondition: (SerialResponse) -> Boolean = { true }
    ) {
        Assert.assertTrue(true)
        val usedIds = mutableSetOf<Long>()
        val exceptionTypes = hashMapOf<Class<*>, Int>()

        var id = initial
        while (true) {
            try {
                val serialData = runBlocking { Web.serial(id) }
                if (breakCondition(serialData.payload.serial))
                    break
            } catch (e: Exception) {
                e.printStackTrace()
                exceptionTypes[e.javaClass] = (exceptionTypes[e.javaClass] ?: 0) + 1
                if (e is NoMoreException)
                    break
            }

            if (usedIds.size == SERIAL_COUNT)
                break

            usedIds += id
            while (usedIds.contains(id))
                id = next(id)
        }

        println("Finished for ${usedIds.size} requests")
        println("Next exceptions was thrown $exceptionTypes")
        println("Id's was requested = $usedIds")
    }

    @Test
    fun checkGson() {
        val ep = EpisodeResponse(43)
        val pl = EpisodePayload(ep)
        val dt = EpisodeData(pl, "view", "url")
        val gson = Gson()
        val string = gson.toJson(dt)
        val data = gson.fromJson(string, EpisodeData::class.java)
        Assert.assertEquals(data, dt)
    }
}

class NoMoreException(message: String) : Exception(message)